var gulp = require('gulp'),
		sass = require('gulp-sass'),
		autoprefixer = require('gulp-autoprefixer'),
		gulpSequence = require('gulp-sequence'),
		uglify = require('gulp-uglify'),
		rjs = require('gulp-requirejs');

gulp.task('sass', function () {
	return gulp.src('app/styles/scss/*.scss')
		.pipe(sass())
		.pipe(autoprefixer('last 2 version'))
		.pipe(gulp.dest('app/styles'));
});

gulp.task('scripts', function () {
	var paths = {
    jquery: '../../bower_components/jquery/dist/jquery',
    underscore: '../../bower_components/underscore/underscore',
    backbone: '../../bower_components/backbone/backbone',
    "backbone.wreqr" : "../../bower_components/backbone.wreqr/lib/backbone.wreqr",
    marionette: '../../bower_components/backbone.marionette/lib/backbone.marionette',
    tpl: '../../bower_components/requirejs-tpl/tpl',
    masonry: '../../bower_components/masonry/dist/masonry.pkgd',
		imagesloaded: '../../bower_components/imagesloaded/imagesloaded.pkgd',
    clipboard: '../../bower_components/clipboard/dist/clipboard',
    facebook : '//connect.facebook.net/en_US/sdk',
    ga : '//www.google-analytics.com/analytics',
		googlemaps: '../../bower_components/googlemaps-amd/src/googlemaps',
		async: '../../bower_components/requirejs-plugins/src/async',
		api : 'commons/api',
    router : 'routers/index',
    controller : 'controllers/index',
		routerFilter: '../../bower_components/backbone-route-filter/backbone-route-filter',
    datePicker: '../../vendor/datepicker/js/bootstrap-datepicker',
    wookmark: '../../bower_components/wookmark/wookmark',
    randomcolor: '../../bower_components/randomcolor/randomColor',
    kakao: '../../vendor/kakao.min'
  };
  var shim = {
    jquery: {
      exports: '$'
    },
    facebook: {
      exports: 'FB'
    }
  };

	gulp.src('bower_components/requirejs/require.js')
		.pipe(uglify())
		.pipe(gulp.dest('dist/scripts'));

	rjs({
		baseUrl: 'app/scripts',
		name: 'main',
		out: 'main.js',
		optimize: 'none',

		paths: paths,
		shim: shim,
		waitSeconds: 0,

		include: [
			'views/layouts/home',
			'views/layouts/reportCreate',
			'views/layouts/reportSearch',
			'views/layouts/reportDetail',
			'views/layouts/dashboard',
      'views/layouts/about',
      'views/layouts/insights'
		]
	}).pipe(uglify()).pipe(gulp.dest('dist/scripts'));
});

gulp.task('build', function (callback) {
	var tasks = ['sass', 'scripts'];
	tasks.push(callback);
	gulpSequence.apply(this, tasks);
});

gulp.task('serve', ['sass'], function (callback) {
	gulp.watch('app/styles/scss/*.scss',['sass']);
});

gulp.task('default', function (callback) {
	gulp.start('build');
});
