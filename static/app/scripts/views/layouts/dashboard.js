define([
  'marionette',
  'backbone',
  'views/layouts/reportsWithModal',
  'tpl!templates/layouts/dashboard.tpl',
  'models/user',
  'views/items/dashboard.userProfile',
  'views/composites/dashboard.recentReports',
  'views/composites/dashboard.myReports'
], function (
    Marionette,
    Backbone,
    ReportsWithModal,
    tpl,
    UserModel,
    UserProfileView,
    RecentReportsView,
    UserReportsView,
    ReportModel,
    ReportDetailView) {
  'use strict';

  return ReportsWithModal.extend({
    template: tpl,
    regions: {
      userProfile: '#user-profile-region',
      recentReports: '#recent-reports-region',
      userReports: '#user-reports-region',
      reportModal: '#report-modal-region'
    },
    childEvents: {
      'report:clicked': 'viewReport',
      'report:closed': 'closeReport'
    },
    onShow: function () {
      var self = this;
      self.userProfile.show(new UserProfileView());
      self.userReports.show(new UserReportsView());

      $('body').delegate('#report-modal-region', 'click', function(event) {
        self.closeReport();
      });

      $('body').delegate('.report-detail-inner', 'click', function(event) {
        event.stopPropagation();
      });

    },
    initialize: function () {
      var self = this;
    }
  })
})
