define([
  'marionette',
  'tpl!templates/layouts/insights.tpl'
], function (Marionette, tpl, InsightsView) {
  'use strict';

  return Marionette.LayoutView.extend({
    template: tpl,
    regions: {
    },
    ui: {
      btnFacebook: '.btn-facebook',
      btnTweet: '.btn-tweet'
    },
    events: {
      'click @ui.btnFacebook': 'shareFacebook',
      'click @ui.btnTweet': 'shareTweet'
    },
    shareFacebook: function (event) {
      event.stopPropagation();
      var self = this;
      var link = 'http://www.roadmob.ph';
      if (G.loadedFB) {
        FB.ui({
          method: 'share',
          href: link
        });
      }
    },
    shareTweet: function (event) {
      var self = this;
      var link = 'http://www.roadmob.ph';
      var x = screen.width/2 - 700/2;
      var y = screen.height/2 - 485/2;
      window.open(encodeURI(link), "Roadmob Twitter Share", 'height=485,width=700,left='+x+',top='+y);
      event.stopPropagation();
    },
    onShow: function () {
      var self = this;
      
    }
  })
})
