define([
  'marionette',
  'backbone',
  'models/report',
  'views/items/reportDetail.report'
], function (
    Marionette,
    Backbone,
    ReportModel,
    ReportDetailView) {
  'use strict';

  return Marionette.LayoutView.extend({
    viewReport: function (child) {
      var self = this;
      var report = new ReportModel({id: child.model.get('id')});
      var reportDetail = new ReportDetailView({model: report})
      reportDetail.model.fetch();
      self.reportModal.show(reportDetail);
      self.reportModal.$el.removeClass('hidden');
      $('body').css('position', 'fixed');
      $('body').css('overflow', 'hidden');
    },
    closeReport: function (child) {
      var self = this;
      self.reportModal.$el.addClass('hidden');
      self.reportModal.reset();
      $('body').css('position', 'relative');
      $('body').css('overflow-x', 'hidden');
      $('body').css('overflow-y', 'auto');
    }
  })
})
