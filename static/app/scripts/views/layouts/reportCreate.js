define([
  'marionette',
  'tpl!templates/layouts/reportCreate.tpl',
  'views/composites/report.form',
  'views/items/confirmSubmitModal',
  'vent'
], function (
  Marionette,
  tpl,
  ReportCreateView,
  ConfirmSubmitModalView,
  vent) {
  'use strict';

  return Marionette.LayoutView.extend({
    template: tpl,
    regions: {
      reportCreate: '#report-create-region',
      modalRegion: '#modal-overlay'
    },
    childEvents: {
      'submit:cancel': 'closeDialog'
    },
    initialize: function () {
      var self = this;
      self.listenTo(vent,'showDialog:confirmSubmit', self.showDialog);
    },
    showDialog: function () {
      var self = this;
      self.modalRegion.$el.addClass('modal-overlay-visible');
      self.modalRegion.show(new ConfirmSubmitModalView());
    },
    closeDialog: function () {
      var self = this;
      self.modalRegion.$el.removeClass('modal-overlay-visible');
      self.modalRegion.empty();

    },
    onShow: function () {
      var self = this;
      self.reportCreate.show(new ReportCreateView());

      $('body').delegate('#modal-overlay', 'click', function(event) {
        self.closeDialog();
      });

      $('body').delegate('.modal-fixed-center', 'click', function(event) {
        event.stopPropagation();
      });
    }
  })
})
