define([
  'marionette',
  'tpl!templates/layouts/reportDetail.tpl',
  'views/items/reportDetail.report',
  'models/report'
], function (Marionette, tpl, ReportDetailView, ReportModel) {
  'use strict';

  return Marionette.LayoutView.extend({
    template: tpl,
    regions: {
      reportDetail: '#report-detail-region'
    },
    params: null,
    initialize: function (data) {
      var self = this;
      self.report_id = data.report_id;
    },
    onShow: function () {
      var self = this;
      self.model = new ReportModel({id: self.report_id});
      self.model.fetch();
      self.reportDetail.show(new ReportDetailView({model: self.model}));
    }
  });
})
