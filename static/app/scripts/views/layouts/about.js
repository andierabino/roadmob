define([
  'marionette',
  'tpl!templates/layouts/about.tpl',
  'views/items/home.howItWorks'
], function (Marionette, tpl, AboutView) {
  'use strict';

  return Marionette.LayoutView.extend({
    template: tpl,
    regions: {
      about: '#about-region'
    },
    onShow: function () {
      var self = this;
      self.about.show(new AboutView());
    }
  })
})
