define([
  'marionette',
  'views/layouts/reportsWithModal',
  'tpl!templates/layouts/reportSearch.tpl',
  'views/composites/search.reports'
], function (Marionette, ReportsWithModal, tpl, ReportSearchView) {
  'use strict';

  return ReportsWithModal.extend({
    template: tpl,
    regions: {
      reportSearch: '#report-search-region',
      reportModal: '#report-modal-region'
    },
    childEvents: {
      'report:clicked': 'viewReport',
      'report:closed': 'closeReport'
    },
    params: null,
    initialize: function (params) {
      var self = this;
      self.params = params;
      delete self.params['destroyImmediate'];
    },
    onShow: function () {
      var self = this;
      self.reportSearch.show(new ReportSearchView(self.params));

      $('body').delegate('#report-modal-region', 'click', function(event) {
        self.closeReport();
      });

      $('body').delegate('.report-detail-inner', 'click', function(event) {
        event.stopPropagation();
      });
    }
  });
})
