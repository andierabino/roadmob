define([
  'marionette',
  'backbone',
  'views/layouts/reportsWithModal',
  'tpl!templates/layouts/home.tpl',
  'views/composites/home.reports',
  'vent',
  'views/items/home.loginModal',
  'views/items/home.hero',
  'views/items/home.liveReports',
  'views/items/home.howItWorks'
], function (
    Marionette,
    Backbone,
    ReportsWithModal,
    tpl,
    ReportCollectionView,
    vent,
    LoginModalView,
    HeroView,
    LiveReportsView,
    HowItWorksView
  ) {
  'use strict';

  return ReportsWithModal.extend({
    template: tpl,
    initialize: function () {
      this.listenTo(vent, 'show:loginModal', this.showLoginModal);
      // $('#header-region').addClass('on-landing');
    },
    regions: {
      reportCollection: '#report-collection-region',
      about: '#about-panel',
      contact: '#contact-panel',
      overlay: '#modal-overlay',
      hero: '#hero-region',
      liveReports: '#live-reports-region',
      howItWorks: '#how-it-works-region',
      reportModal: '#report-modal-region'
    },
    ui: {
      searchInput: '#search-input',
      searchButton: '#search-button',
      fileReportButton: '#file-main'
    },
    events: {
      'click @ui.searchButton': 'search',
      'click @ui.fileReportButton': 'fileReport',
      'keyup @ui.searchInput': 'searchOnKeyup'
    },
    childEvents: {
      'report:clicked': 'viewReport',
      'report:closed': 'closeReport'
    },
    fileReport: function (e) {
      e.preventDefault();
      ga('send', 'event', 'fileReport');
      vent.trigger('checklogin:filereport', e);
      return false;
    },
    searchOnKeyup: function (e) {
      var self = this;
      if (e.which === 13) {
        self.search();
      }
    },
    search: function () {
      var self = this;
      var query = self.ui.searchInput.val();
      if (query) {
        Backbone.history.navigate('search?q='+query, {trigger: true, replace: false});
      }
    },
    onShow: function () {
      var self = this;
      self.reportCollection.show(new ReportCollectionView());
      self.hero.show(new HeroView());
      //self.liveReports.show(new LiveReportsView());
      self.howItWorks.show(new HowItWorksView());

      $('body').delegate('#modal-overlay,.modal-close', 'click', function(event) {
        event.stopPropagation();
        self.hideModal();
      });

      $('body').delegate('#close-notice', 'click', function(event) {
        event.stopPropagation();
        $('.footer-notice').remove();
      });

      $('body').delegate('#report-modal-region', 'click', function(event) {
        self.closeReport();
      });

      $('body').delegate('.report-detail-inner', 'click', function(event) {
        event.stopPropagation();
      });

    },
    showLoginModal: function () {
      var self = this;
      $('#modal-overlay').addClass('modal-overlay-visible');
      self.overlay.show(new LoginModalView());
    },
    hideModal: function () {
      $('.modal-fixed-center').fadeOut(500);
      $('#modal-overlay').removeClass('modal-overlay-visible');
    }
  })
})
