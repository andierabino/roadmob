define([
  'marionette',
  'backbone',
  'tpl!templates/composites/dashboard.recentReports.tpl',
  'views/items/dashboard.report',
  'models/reports'
], function (Marionette, Backbone, tpl, ReportItemView, ReportCollection) {
  return Marionette.CompositeView.extend(
    {
      template: tpl,
      childView: ReportItemView,
      childViewContainer: '#dashboard-reports-collection',
      ui: {
        reportsLoader: '.reports-loading',
        seeMore: '.see-more',
        noReports: '.no-reports'
      },
      events: {
        'click @ui.seeMore': 'loadMore'
      },
      initialize: function () {
        var self = this;

        self.collection = new ReportCollection();

        self.collection.fetch({
          data: {
            limit: 8,
            user: 'others'
          },
          success: function (collection) {
            if(collection.length == 0){
              self.ui.noReports.show();
            }
          },
          complete: function () {

          }
        });
      },
      onRender: function () {
        var self = this;

        self.showHasMore();
      },
      showHasMore: function () {
        var self = this;
        if (!self.collection.has_more) {
          self.ui.seeMore.hide();
        } else {
          self.ui.seeMore.show();
        }
      },
      loadMore: function () {
        var self = this;
        var lastModel = self.collection.at(self.collection.length - 1);
        var lastDate = lastModel.get('created_at','');
        self.collection.fetch({
          remove: false,
          data: {
            limit: 3,
            last: lastDate
          },
          success: function (collection) {

          },
          complete: function () {
            self.showHasMore();
          }
        });
      }
    }

  )
});
