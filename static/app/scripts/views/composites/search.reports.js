define([
  'marionette',
  'tpl!templates/composites/search.reports.tpl',
  'views/items/search.report',
  'models/reports',
  'wookmark',
  'commons/util'
], function (Marionette, tpl, ReportItemView, ReportCollection, Wookmark, util) {
  return Marionette.CompositeView.extend({
    template: tpl,
    childView: ReportItemView,
    childViewContainer: '#reports-collection',
    wookmark: null,
    ui: {
      searchButton: '#roadmob-search-button',
      searchInput: '#roadmob-search-input',
      searchResultsText: '#search-results-text'
    },
    events: {
      'click #roadmob-search-input': 'search',
      'keyup #roadmob-search-button': 'searchOnKeyup'
    },
    searchOnKeyup: function (e) {
      var self = this;
      if (e.which === 13) {
        self.search();
      }
    },
    setupWookmark: function () {
      var self = this;
      var container = '#reports-collection';
      self.wookmark = new Wookmark(container, {
        direction: 'left',
        align: 'left',
        autoResize: true, // This will auto-update the layout when the browser window is resized.
        itemWidth: function () {
          // Return a maximum width depending on the viewport
          var windowWidth = util.getWindowWidth();
          var collectionWidth = $('#reports-collection').width();
          if (windowWidth > 800) {
            return 0.25 * collectionWidth;
          } else if (windowWidth <= 420) {
            return collectionWidth;
          } else {
            return 0.5 * collectionWidth;
          }
        },
        flexibleWidth: function () {
          // Return a maximum width depending on the viewport
          return '100%';
        },
        offset: 0
      });
    },
    search: function () {
      var self = this;
      var query = self.ui.searchInput.val();
      if (query) {
        self.collection.fetch({
          data: {q: query},
          processData: true,
          beforeSend: function () {
            $('#preloader-overlay').removeClass('hidden');
          },
          complete: function () {
            $('#preloader-overlay').addClass('hidden');
            self.ui.searchInput.blur();
            self.setupWookmark();
          }
        });
      }
    },
    initialize: function (params) {
      var self = this;
      self.params = params;
      self.collection = new ReportCollection();
      if ('q' in params) {
        self.collection.fetch({
          data: {q: params['q']},
          processData: true,
          beforeSend: function () {
            $('#preloader-overlay').removeClass('hidden');
          },
          success: function (collection) {
            self.updateResultsText(collection.length);
          },
          complete: function () {
            $('#preloader-overlay').addClass('hidden');
            self.setupWookmark();
          }
        });
      }

    },
    updateResultsText: function (count) {
      var self = this;
      var text = count <= 1 ? 'result' : 'results';
      self.ui.searchResultsText.text('Found ' + count + ' ' + text + '.');
    },
    onRender: function () {
      var self = this;
      if ('q' in self.params) {
        var searchInput = document.getElementById('roadmob-search-input');
        searchInput.value = self.params['q'];
        //searchInput.focus();
      }
    }
  });
})
