define([
  'marionette',
  'backbone',
  'tpl!templates/composites/report.form.tpl',
  'datePicker',
  'vent',
  'commons/util'
], function (Marionette, Backbone, tpl, _datepicker, vent, util) {
  return Marionette.CompositeView.extend({
    template: tpl,

    ui: {
      submitBtn: '#submit-report',
      inputPlateNumber: 'input[name="plate_number"]',
      inputOperator: 'input[name="operator"]',
      inputVehicleType: 'select[name="vehicle_type"]',
      //the commented selector will not select anything!
      //inputComplaintType: 'select[name="complaint_type"]',
      inputComplaintType: 'select[id="complaint_type"]',
      inputDate: 'input[name="date"]',
      inputTime: 'input[name="time"]',
      inputLocation: 'input[name="location"]',
      inputDescription: 'textarea[name="description"]',
      inputContact: 'input[name="contact"]',
      inputEmail: 'input[name="email"]',
      inputPicture: 'input[name="picture"]',
      clearUploadBtn: '.clear-upload'
    },
    events: {
      'focus input': 'resetInput',
      'focus textarea': 'resetInput',
      'focus select': 'resetInput',
      'change @ui.inputPicture': 'showImagePreview',
      'click @ui.clearUploadBtn': 'clearUploadPreview',
      'click @ui.submitBtn': 'validate',
    },
    form: {
      inputPlateNumber: ['required', 'platenumber'],
      inputOperator: ['alphanumspecial'],
      inputVehicleType: ['required'],
      inputComplaintType: ['required'],
      inputDate: ['required'],
      inputLocation: ['required', 'alphanumspecial'],
      inputDescription: ['alphanumspecial'],
      inputContact: ['required', 'num'],
      inputEmail: ['required', 'email']
    },
    optional: [
      'inputDescription',
      'inputOperator'
    ],
    rules: {
      required: function (str) {
        return str!='' && str!=null;
      },
      alphanumspecial: function (str) {
        var pattern = /^[ A-Za-z0-9?!"\(\)\/\n_@'.,:\#&+-ñáéíóúü]+$/i;
        return pattern.test(str);
      },
      num: function (str) {
        var pattern = /^\d+$/;
        return pattern.test(str);
      },
      platenumber: function (str) {
        var pattern = /^[A-za-z]{2,3}\-*[0-9]{3,5}$/;
        return pattern.test(str);
      },
      email: function (str) {
        var pattern = new RegExp('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$', 'i');
        return pattern.test(str);
      }
    },
    errorMessages : {
      required: "This field may not be blank.",
      alphanumspecial: "This field must only contain spaces, alphanumeric characters or _@'./#&+-():",
      platenumber: "A valid plate number is required.",
      num: "This field must only contain numbers.",
      email: "Email address must be valid."
    },
    onRender: function () {

      var self = this;
      require(['googlemaps!'], function(googleMaps) {
        var input = /** @type {!HTMLInputElement} */(
              document.getElementById('location'));
        var autocomplete = new googleMaps.places.Autocomplete(input);
      });

      self.ui.inputDate.datepicker({
        format: 'yyyy-mm-dd'
      });

      $.ajax({
        method: 'GET',
        url: '/api/user/contact_details',
        success: function (data) {
          self.ui.inputContact.val(data['contact']);
          self.ui.inputEmail.val(data['email']);
        }
      });
    },

    validate: function () {

      var self = this;
      var invalid = 0;
      var firstError = null;
      $.each(self.form, function (key, val) {
        var validate = true;
        var str = self.ui[key].val();
        if (str==="") {
          if (self.optional.indexOf(key) > -1) {

            validate = false;
          }
        }
        if (validate) {
          $.each(val, function (i, rule) {
            var valid = self.rules[rule](str);

            if (!valid) {
              invalid++;

              if (firstError == null) {
                firstError = self.ui[key].offset().top - 200;
              }
              self.ui[key].addClass('input-danger');

              if (self.ui[key].next('.error-text').length) {
                self.ui[key].next('.error-text').html(self.errorMessages[rule]);
              } else {
                self.ui[key].after('<div class="grid-100 grid-parent error-text">'+self.errorMessages[rule] +'</div>');
              }

              return false;
            }
          })
        }

      });
      if (invalid) {
        $('html, body').animate({
            scrollTop: firstError
        }, 300);
        return false;
      } else {
        self.submit();
      }
    },
    submit: function () {

      var self = this;


      var data = new FormData();
      data.append('report', JSON.stringify({
          plate_number: self.ui.inputPlateNumber.val(),
          vehicle_type: self.ui.inputVehicleType.val(),
          operator: self.ui.inputOperator.val(),
          incident: self.ui.inputComplaintType.val(),
          date: self.ui.inputDate.val() + 'T00:00:00',
          location: self.ui.inputLocation.val(),
          description: self.ui.inputDescription.val()
        }))

      data.append('userinfo', JSON.stringify({
          contact: self.ui.inputContact.val(),
          email: self.ui.inputEmail.val()
        }))
      data.append('picture', $('input[name="picture"]')[0].files[0]);

      var csrftoken = util.getCookie('csrftoken');
      $.ajax({
        url: '/api/report',
        method: 'POST',
        contentType: false,
        processData: false,
        data: data,
        beforeSend: function (xhr, settings) {
          $('#preloader-overlay').removeClass('hidden');
          xhr.setRequestHeader("X-CSRFToken", csrftoken);
        },
        success: function () {
          router.navigate('dashboard', {trigger: true});
          vent.trigger('report:success','You have successfully submitted your report!');
        },
        error: function (xhr, ajaxOptions, thrownError) {
          $('.modal-region').addClass('hidden');
          var data = $.parseJSON(xhr.responseText);
          var firstError = null;
          for (var key in data) {
            console.log(key);
            var errors = data[key];
            $('.error-'+key).html();
            for (var i in errors) {
              $('input#'+key).addClass('input-danger');
              if (firstError == null) {
                firstError = $('input#'+key).offset().top;
              }
              if ($('input#'+key).next('.error-text').length) {
                $('input#'+key).next('.error-text').html(self.errorMessages[rule]);
              } else {
                $('input#'+key).after('<div class="grid-100 grid-parent error-text">'+errors[i] + '\n' +'</div>');
              }
            }
          }
          $('html, body').animate({
              scrollTop: firstError - 200
          }, 300);
        },
        complete: function () {
          $('#preloader-overlay').addClass('hidden');
        }
      });

    },
    resetInput: function (e) {
      $(e.target).removeClass('input-danger');
      $(e.target).next('.error-text').html('');
    },
    showImagePreview: function (event) {
      var self = this;
      var file = event.target.files[0];
      var output = document.getElementById('upload-preview');
      var imgsrc = URL.createObjectURL(file);

      if(file.size>=2*1024*1024) {
          alert("Image size exceed of maximum of 2mb");
          output.remove();
          self.ui.clearUploadBtn.addClass('hidden');
          $(event.target).after('<img id="upload-preview">');
          return;
      }

      if(!file.type.match('image/jp.*')&&!file.type.match('image/png.*')) {
          alert("You are only allowed to upload jpeg/png files.");
          output.remove();
          self.ui.clearUploadBtn.addClass('hidden');
          $(event.target).after('<img id="upload-preview">');
          return;
      }

      if (imgsrc) {
        output.src = imgsrc;
        self.ui.clearUploadBtn.removeClass('hidden');
      } else {
        output.remove();
        self.ui.clearUploadBtn.addClass('hidden');
        $(event.target).after('<img id="upload-preview">');
      }
    },
    clearUploadPreview: function (event) {
      var self = this;
      $('#upload-preview').remove();
      $('.upload-preview-wrapper').append('<img id="upload-preview">');
      self.ui.clearUploadBtn.addClass('hidden');
      self.ui.inputPicture.val('');
    }
  });
})
