define('window', function () {
  return window;
});

define('document', function () {
  return document;
});
define([
  'marionette',
  'tpl!templates/composites/home.reports.tpl',
  'views/items/home.report',
  'models/reports',
  'wookmark',
  'commons/util'
], function (Marionette, tpl, ReportItemView, ReportCollection, Wookmark, util) {
  function getWindowWidth() {
    return Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
  }
  return Marionette.CompositeView.extend(
    {
      template: tpl,
      className: 'grid-100',
      childView: ReportItemView,
      childViewContainer: '#reports-collection',
      ui: {
        reportCollection: '#reports-collection',
        reportsLoader: '.reports-loading',
        seeMore: '.see-more'
      },
      events: {
        'click @ui.seeMore': 'loadMore'
      },
      templateHelpers: function () {
        var self = this;
        return {
            total: self.collection.length
        };
      },
      updateCount: function() {
          var self = this;
          self.$('.count_span').html(self.collection.length);
      },
      wookmark: null,
      initialize: function () {
        var self = this;
        self.collection = new ReportCollection();
        self.collection.on('reset', self.updateCount, self);
        self.collection.on('remove', self.updateCount, self);
        self.collection.on('add', self.updateCount, self);
        self.collection.fetch({
          data: {
            limit: 4
          },
          success: function (collection) {
            self.ui.reportsLoader.hide();

            var container = '#reports-collection';
            self.wookmark = new Wookmark(container, {
              align: 'left',
              offset: 0,
              outerOffset: 0,
              autoResize: true, // This will auto-update the layout when the browser window is resized.
              itemWidth: function () {
                // Return a maximum width depending on the viewport
                var windowWidth = util.getWindowWidth();
                var collectionWidth = $('#reports-collection').width();
                if (windowWidth > 800) {
                  return 0.25 * collectionWidth;
                } else if (windowWidth <= 420) {
                  return collectionWidth;
                } else {
                  return 0.5 * collectionWidth;
                }
              },
              flexibleWidth: function () {
                // Return a maximum width depending on the viewport
                return '100%';
              },
              container: $("#reports-collection"),
              resizeDelay: 50

            });
            self.ui.reportCollection.removeClass('opaque');
            self.showHasMore();
          }
        });
      },
      onRender: function () {
        var self = this;
        self.showHasMore();
      },
      showHasMore: function () {
        var self = this;
        if (!self.collection.has_more) {
          self.ui.seeMore.hide();
        } else {
          self.ui.seeMore.show();
        }
      },
      loadMore: function () {
        var self = this;
        var lastModel = self.collection.at(self.collection.length - 1);
        var lastDate = lastModel.get('created_at','');
        self.collection.fetch({
          remove: false,
          data: {
            limit: 4,
            last: lastDate
          },
          beforeSend: function () {
            $('#preloader-overlay').removeClass('hidden');
          },
          success: function (collection) {

          },
          complete: function () {
            self.showHasMore();
            self.wookmark.initItems();
            self.wookmark.layout(true);
            $('#preloader-overlay').addClass('hidden');
          }
        });
      }
    }

  )
});
