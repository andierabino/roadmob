define([
  'marionette',
  'backbone',
  'tpl!templates/items/home.liveReports.tpl',
  'vent'
], function (Marionette, Backbone, tpl, vent) {

  return Marionette.ItemView.extend({
    template: tpl,
    className: 'live-reports-wrapper'
  });
});
