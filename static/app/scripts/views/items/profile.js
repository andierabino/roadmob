define([
  'marionette',
  'backbone',
  'tpl!templates/layouts/home.tpl',
  'models/user',
  'vent',
  'views/items/menu'
], function (Marionette, Backbone, tpl, UserModel, vent, MenuView) {
  'use strict';

  var instance = null;
  var profile = Marionette.LayoutView.extend({
    template: tpl,
    initialize: function () {
      var self = this;
      self.model = UserModel.getLoggedUser();
      self.listenTo(self.model,"change",self.render);
      self.listenTo(self.model,"change",self.removeBanner);
    },

    className: 'grid-container',
    ui: {
      //searchButton: '#roadmob-search-button',
      //searchInput: '#roadmob-search-input',
      //fbLoginButton: '#fb-login',
      //userToggle: '.menu-button',
      //userOptions: '.link-drop-down'
    },
    events: {
      //'click @ui.searchButton': 'search',
      //'keyup @ui.searchInput': 'searchOnKeyup',
      //'click @ui.fbLoginButton': 'fbLogin',
      //'click @ui.logoutButton': 'logout',
      '//click @ui.userToggle': 'showMenu'
    },

    fbLogin: function () {
      vent.trigger('login');
    },
    modelEvents : {
      'change' : 'render'
    },
    search: function () {
      var self = this;
      var query = self.ui.searchInput.val();
      if (query)
        Backbone.history.navigate('search?q='+query, {trigger: true, replace: false});
    },
    showMenu: function () {
      var self = this;
      $('#site-wrapper').toggleClass('show-nav');

    },
    hideMenu: function () {
      $('#site-wrapper').removeClass('show-nav');
    },

    removeBanner: function()
    {
      $('#banner-section').addClass('hidden');
      console.log('I was called');
    },

    searchOnKeyup: function (e) {
      var self = this;
      if (e.which === 13) {
        self.search();
      }
      if (e.which === 8) {
        if(!self.ui.searchInput.val()) {
          //Backbone.history.navigate('/', {trigger: true, replace: false});
        }
      }
    }
  }, {
    getInstance: function () {
      if (instance == null) {
        instance = new header();
      }

      return instance;
    }
  });

  return header;
});
