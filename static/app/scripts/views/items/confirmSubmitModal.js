define([
  'marionette',
  'backbone',
  'tpl!templates/items/confirmSubmitModal.tpl',
  'vent'
], function (Marionette, Backbone, tpl, vent) {

  return Marionette.ItemView.extend({
    template: tpl,
    className: 'modal-fixed-center',
    ui: {
      confirmButton: '#confirm',
      cancelButton: '#cancel',
      closeButton: '.modal-close'
    },
    events: {
      'click @ui.confirmButton': 'confirm'
    },
    triggers: {
      'click @ui.cancelButton': 'submit:cancel',
      'click @ui.closeButton': 'submit:cancel'
    },
    fbLogin: function (e) {
      vent.trigger('login', e);
    }
  });
});
