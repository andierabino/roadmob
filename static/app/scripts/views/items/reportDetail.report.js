define([
  'marionette',
  'backbone',
  'views/items/report',
  'tpl!templates/items/reportDetail.report.tpl'
], function (Marionette, Backbone, ReportView, tpl) {

  return ReportView.extend({
    template: tpl,
    ui: {
      shareFB: '.share-facebook',
      shareTwitter: '.share-twitter',
      shareKakao: '.share-kakao',
      reportClose: '.modal-close'
    },
    events: {
      'click @ui.shareFB': 'shareFB',
      'click @ui.shareTwitter': 'shareTwitter',
      'click @ui.shareKakao': 'shareKakao'
    },
    modelEvents : {
      'change' : 'render',
      'sync' : 'render'
    },
    triggers: {
        "click @ui.reportClose": "report:closed"
    },
    onRender: function () {
      var self = this;
      if(G.loadedFB)
        FB.XFBML.parse();
      if (Backbone.history.getFragment().indexOf('report') != -1) {
        self.ui.reportClose.remove();
      }
      if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        self.ui.shareKakao.remove();
      }
    }
  });
});
