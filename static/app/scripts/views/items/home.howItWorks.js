define([
  'marionette',
  'backbone',
  'tpl!templates/items/home.howItWorks.tpl',
  'vent'
], function (Marionette, Backbone, tpl, vent) {

  return Marionette.ItemView.extend({
    template: tpl,
    className: 'how-it-works-wrapper'
  });
});
