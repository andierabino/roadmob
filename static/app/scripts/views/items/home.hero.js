define([
  'marionette',
  'backbone',
  'tpl!templates/items/home.hero.tpl',
  'vent'
], function (Marionette, Backbone, tpl, vent) {
  var amStyle = [

      {
          "featureType": "administrative",
          "elementType": "labels.text.fill",
          "stylers": [
              {
                  "color": "#6195a0"
              }
          ]
      },
      {
          "featureType": "administrative.province",
          "elementType": "geometry.stroke",
          "stylers": [
              {
                  "visibility": "off"
              }
          ]
      },
      {
          "featureType": "landscape",
          "elementType": "geometry",
          "stylers": [
              {
                  "lightness": "0"
              },
              {
                  "saturation": "0"
              },
              {
                  "color": "#f5f5f2"
              },
              {
                  "gamma": "1"
              }
          ]
      },
      {
          "featureType": "landscape.man_made",
          "elementType": "all",
          "stylers": [
              {
                  "lightness": "-3"
              },
              {
                  "gamma": "1.00"
              }
          ]
      },
      {
          "featureType": "landscape.natural.terrain",
          "elementType": "all",
          "stylers": [
              {
                  "visibility": "off"
              }
          ]
      },
      {
          "featureType": "poi",
          "elementType": "all",
          "stylers": [
              {
                  "visibility": "off"
              }
          ]
      },
      {
          "featureType": "poi.park",
          "elementType": "geometry.fill",
          "stylers": [
              {
                  "color": "#bae5ce"
              },
              {
                  "visibility": "on"
              }
          ]
      },
      {
          "featureType": "road",
          "elementType": "all",
          "stylers": [
              {
                  "saturation": -100
              },
              {
                  "lightness": 45
              },
              {
                  "visibility": "simplified"
              }
          ]
      },
      {
          "featureType": "road.highway",
          "elementType": "all",
          "stylers": [
              {
                  "visibility": "simplified"
              }
          ]
      },
      {
          "featureType": "road.highway",
          "elementType": "geometry.fill",
          "stylers": [
              {
                  "color": "#fac9a9"
              },
              {
                  "visibility": "simplified"
              }
          ]
      },
      {
          "featureType": "road.highway",
          "elementType": "labels.text",
          "stylers": [
              {
                  "color": "#4e4e4e"
              }
          ]
      },
      {
          "featureType": "road.arterial",
          "elementType": "labels.text.fill",
          "stylers": [
              {
                  "color": "#787878"
              }
          ]
      },
      {
          "featureType": "road.arterial",
          "elementType": "labels.icon",
          "stylers": [
              {
                  "visibility": "off"
              }
          ]
      },
      {
          "featureType": "transit",
          "elementType": "all",
          "stylers": [
              {
                  "visibility": "simplified"
              }
          ]
      },
      {
          "featureType": "transit.station.airport",
          "elementType": "labels.icon",
          "stylers": [
              {
                  "hue": "#0a00ff"
              },
              {
                  "saturation": "-77"
              },
              {
                  "gamma": "0.57"
              },
              {
                  "lightness": "0"
              }
          ]
      },
      {
          "featureType": "transit.station.rail",
          "elementType": "labels.text.fill",
          "stylers": [
              {
                  "color": "#43321e"
              }
          ]
      },
      {
          "featureType": "transit.station.rail",
          "elementType": "labels.icon",
          "stylers": [
              {
                  "hue": "#ff6c00"
              },
              {
                  "lightness": "4"
              },
              {
                  "gamma": "0.75"
              },
              {
                  "saturation": "-68"
              }
          ]
      },
      {
          "featureType": "water",
          "elementType": "all",
          "stylers": [
              {
                  "color": "#eaf6f8"
              },
              {
                  "visibility": "on"
              }
          ]
      },
      {
          "featureType": "water",
          "elementType": "geometry.fill",
          "stylers": [
              {
                  "color": "#c7eced"
              }
          ]
      },
      {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [
              {
                  "lightness": "-49"
              },
              {
                  "saturation": "-53"
              },
              {
                  "gamma": "0.79"
              }
          ]
      }
  ];
  var pmStyle = [
      {
          "stylers": [
              {
                  "hue": "#ff1a00"
              },
              {
                  "invert_lightness": true
              },
              {
                  "saturation": -100
              },
              {
                  "lightness": 46
              },
              {
                  "gamma": 0.5
              }
          ]
      },
      {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
              {
                  "color": "#2D333C"
              }
          ]
      },
      {
        "featureType": "all",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#777777"
          }
        ]
      },
      {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
          { "visibility": "simplified" }
        ]
      },
      {
        "featureType": "poi.business",
        "elementType": "labels",
        "stylers": [
          { "visibility": "off" }
        ]
      }
  ];


  var gifLanding = [
        {
            "featureType": "all",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "saturation": 36
                },
                {
                    "color": "#000000"
                },
                {
                    "lightness": 40
                },
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "visibility": "off"
                },
                {
                    "color": "#000000"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 17
                },
                {
                    "weight": 1.2
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 21
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 17
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 29
                },
                {
                    "weight": 0.2
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 18
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 19
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 17
                }
            ]
        }
    ];


  return Marionette.ItemView.extend({
    template: tpl,
    className: 'hero-gmap-container',
    initialize: function() {
      require([''], function() {
        window.googleMapLoaded = true;
        // var d = new Date();
        // var h = d.getHours();
        // var styles = (h >= 6 && h < 18) ? amStyle : pmStyle;
        // if(h >= 6 && h < 18) {
        //   $('.hero-overlay').addClass('daytime');
        //   $('#header-region').removeClass('on-landing');
        // }
        var styles = gifLanding;
        var styledMap = new google.maps.StyledMapType(styles,
          { name: "Styled Map" });

        var map = new google.maps.Map(document.getElementsByClassName('hero-map')[0], {
          mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
          },
          center: {lat: 14.5797168, lng: 121.0577723},
          zoom: 16,
          disableDefaultUI: true,
          scrollwheel: false,
          navigationControl: false,
          mapTypeControl: false,
          scaleControl: false,
          draggable: false,
          disableDoubleClickZoom: true,
          mapTypeId: google.maps.MapTypeId.TERRAIN
        });

        // Define the symbol, using one of the predefined paths ('CIRCLE')
        // supplied by the Google Maps JavaScript API.
        var lineSymbol = {
          path: google.maps.SymbolPath.CIRCLE,
          fillColor: '#fff',
          fillOpacity: 1,
          scale: 3,
          strokeColor: '#393',
          strokeOpacity: 0,
          strokeWidth: 0,
        };

        // Create the polyline and add the symbol to it via the 'icons' property.
        var edsaCars = new Array();

        // EDSA Northbound #1
        edsaCars[0] = new google.maps.Polyline({
          path: [
            {lat: 14.5713868, lng: 121.0469626},
            {lat: 14.5722543, lng: 121.0473335},
            {lat: 14.5731182, lng: 121.0478408},
            {lat: 14.5813653, lng: 121.0539309},
            {lat: 14.5830964, lng: 121.0550984},
            {lat: 14.5842610, lng: 121.0556352},
            {lat: 14.5870161, lng: 121.0565695}
          ],
          icons: [
            {
              icon: lineSymbol,
              offset: '100%'
            },
          ],
          strokeColor: 'transparent',
          strokeWidth: 0,
          map: map
        });

        // EDSA Southbound #1
        edsaCars[1] = new google.maps.Polyline({
          path: [
            {lat: 14.5870161, lng: 121.0563695},
            {lat: 14.5842610, lng: 121.0553352},
            {lat: 14.5830964, lng: 121.0546984},
            {lat: 14.5813653, lng: 121.0535309},

            {lat: 14.5813653, lng: 121.0535309},
            {lat: 14.5731182, lng: 121.0475408},
            {lat: 14.5722543, lng: 121.0475335},
            {lat: 14.5713868, lng: 121.0470626}
          ],
          icons: [{
            icon: lineSymbol,
            offset: '100%'
          }],
          strokeColor: 'transparent',
          strokeWidth: 0,
          map: map
        });

        // EDSA Northbound #2
        edsaCars[2] = new google.maps.Polyline({
          path: [
            {lat: 14.5713868, lng: 121.0469626},
            {lat: 14.5722543, lng: 121.0473335},
            {lat: 14.5731182, lng: 121.0478408},
            {lat: 14.5813653, lng: 121.0539309},
            {lat: 14.5830964, lng: 121.0550984},
            {lat: 14.5842610, lng: 121.0556352},
            {lat: 14.5870161, lng: 121.0565695}
          ],
          icons: [
            {
              icon: lineSymbol,
              offset: '90%'
            },
          ],
          strokeColor: 'transparent',
          strokeWidth: 0,
          map: map
        });

        // EDSA Southbound #2
        edsaCars[3] = new google.maps.Polyline({
          path: [
            {lat: 14.5870161, lng: 121.0563695},
            {lat: 14.5842610, lng: 121.0553352},
            {lat: 14.5830964, lng: 121.0546984},
            {lat: 14.5813653, lng: 121.0535309},

            {lat: 14.5813653, lng: 121.0535309},
            {lat: 14.5731182, lng: 121.0475408},
            {lat: 14.5722543, lng: 121.0475335},
            {lat: 14.5713868, lng: 121.0470626}
          ],
          icons: [{
            icon: lineSymbol,
            offset: '100%'
          }],
          strokeColor: 'transparent',
          strokeWidth: 0,
          map: map
        });

        // EDSA Southbound #3
        edsaCars[4] = new google.maps.Polyline({
          path: [
            {lat: 14.5870161, lng: 121.0563695},
            {lat: 14.5842610, lng: 121.0553352},
            {lat: 14.5830964, lng: 121.0546984},
            {lat: 14.5813653, lng: 121.0535309},

            {lat: 14.5813653, lng: 121.0535309},
            {lat: 14.5731182, lng: 121.0475408},
            {lat: 14.5722543, lng: 121.0475335},
            {lat: 14.5713868, lng: 121.0470626}
          ],
          icons: [{
            icon: lineSymbol,
            offset: '100%'
          }],
          strokeColor: 'transparent',
          strokeWidth: 0,
          map: map
        });

        // SHAW ROUTES
        var shawCars = new Array();

        shawCars[0] = new google.maps.Polyline({
          path: [
            {lat: 14.5703589, lng: 121.0665305},
            {lat: 14.5739460, lng: 121.0622315},
            {lat: 14.5741802, lng: 121.0621264},
            {lat: 14.5743390, lng: 121.0619437},
            {lat: 14.5744374, lng: 121.0617097},
            {lat: 14.5791610, lng: 121.0561540},
            {lat: 14.5834540, lng: 121.0511420},
            {lat: 14.5848550, lng: 121.0494690},
            {lat: 14.5871630, lng: 121.0465660}
          ],
          icons: [
            {
              icon: lineSymbol,
              offset: '100%'
            },
          ],
          strokeColor: 'transparent',
          strokeWidth: 0,
          map: map
        });
        shawCars[1] = new google.maps.Polyline({
          path: [
            {lat: 14.5871250, lng: 121.0465270},
            {lat: 14.5848430, lng: 121.0493410},
            {lat: 14.5833980, lng: 121.0510200},
            {lat: 14.5774310, lng: 121.0579930},
            {lat: 14.5743610, lng: 121.0615440},
            {lat: 14.5741850, lng: 121.0616880},
            {lat: 14.5739780, lng: 121.0618890},
            {lat: 14.5738680, lng: 121.0621500},
            {lat: 14.5702200, lng: 121.0664880}
          ],
          icons: [
            {
              icon: lineSymbol,
              offset: '100%'
            },
          ],
          strokeColor: 'transparent',
          strokeWidth: 0,
          map: map
        });
        shawCars[2] = new google.maps.Polyline({
          path: [
            {lat: 14.5703589, lng: 121.0665305},
            {lat: 14.5739460, lng: 121.0622315},
            {lat: 14.5741802, lng: 121.0621264},
            {lat: 14.5743390, lng: 121.0619437},
            {lat: 14.5744374, lng: 121.0617097},
            {lat: 14.5791610, lng: 121.0561540},
            {lat: 14.5834540, lng: 121.0511420},
            {lat: 14.5848550, lng: 121.0494690},
            {lat: 14.5871630, lng: 121.0465660}
          ],
          icons: [
            {
              icon: lineSymbol,
              offset: '100%'
            },
          ],
          strokeColor: 'transparent',
          strokeWidth: 0,
          map: map
        });
        shawCars[3] = new google.maps.Polyline({
          path: [
            {lat: 14.5871250, lng: 121.0465270},
            {lat: 14.5848430, lng: 121.0493410},
            {lat: 14.5833980, lng: 121.0510200},
            {lat: 14.5774310, lng: 121.0579930},
            {lat: 14.5743610, lng: 121.0615440},
            {lat: 14.5741850, lng: 121.0616880},
            {lat: 14.5739780, lng: 121.0618890},
            {lat: 14.5738680, lng: 121.0621500},
            {lat: 14.5702200, lng: 121.0664880}
          ],
          icons: [
            {
              icon: lineSymbol,
              offset: '100%'
            },
          ],
          strokeColor: 'transparent',
          strokeWidth: 0,
          map: map
        });
        shawCars[4] = new google.maps.Polyline({
          path: [
            {lat: 14.5871250, lng: 121.0465270},
            {lat: 14.5848430, lng: 121.0493410},
            {lat: 14.5833980, lng: 121.0510200},
            {lat: 14.5774310, lng: 121.0579930},
            {lat: 14.5743610, lng: 121.0615440},
            {lat: 14.5741850, lng: 121.0616880},
            {lat: 14.5739780, lng: 121.0618890},
            {lat: 14.5738680, lng: 121.0621500},
            {lat: 14.5702200, lng: 121.0664880}
          ],
          icons: [
            {
              icon: lineSymbol,
              offset: '100%'
            },
          ],
          strokeColor: 'transparent',
          strokeWidth: 0,
          map: map
        });

        // MERALCO AVE ROUTES
        var meralcoCars = new Array();

        meralcoCars[0] = new google.maps.Polyline({
          path: [
            {lat: 14.5703589, lng: 121.0665305},
            {lat: 14.5739460, lng: 121.0622315},
            {lat: 14.5741802, lng: 121.0621264},
            {lat: 14.5743390, lng: 121.0619437},
            {lat: 14.5744374, lng: 121.0617097},
            {lat: 14.5748960, lng: 121.0611890},
            {lat: 14.5778270, lng: 121.0637010},
            {lat: 14.5799700, lng: 121.0638670},
            {lat: 14.5866650, lng: 121.0639760},
            {lat: 14.5883360, lng: 121.0639360}
          ],
          icons: [
            {
              icon: lineSymbol,
              offset: '100%'
            },
          ],
          strokeColor: 'transparent',
          strokeWidth: 0,
          map: map
        });
        meralcoCars[1] = new google.maps.Polyline({
          path: [
            {lat: 14.5874920, lng: 121.0638270},
            {lat: 14.5839230, lng: 121.0637530},
            {lat: 14.5818140, lng: 121.0637600},
            {lat: 14.5779470, lng: 121.0636180},
            {lat: 14.5749910, lng: 121.0610960},
            {lat: 14.5791610, lng: 121.0561540},
            {lat: 14.5834540, lng: 121.0511420},
            {lat: 14.5848550, lng: 121.0494690},
            {lat: 14.5871630, lng: 121.0465660}
          ],
          icons: [
            {
              icon: lineSymbol,
              offset: '100%'
            },
          ],
          strokeColor: 'transparent',
          strokeWidth: 0,
          map: map
        });
        meralcoCars[2] = new google.maps.Polyline({
          path: [
            {lat: 14.5703589, lng: 121.0665305},
            {lat: 14.5739460, lng: 121.0622315},
            {lat: 14.5741802, lng: 121.0621264},
            {lat: 14.5743390, lng: 121.0619437},
            {lat: 14.5744374, lng: 121.0617097},
            {lat: 14.5748960, lng: 121.0611890},
            {lat: 14.5778270, lng: 121.0637010},
            {lat: 14.5799700, lng: 121.0638670},
            {lat: 14.5866650, lng: 121.0639760},
            {lat: 14.5883360, lng: 121.0639360}
          ],
          icons: [
            {
              icon: lineSymbol,
              offset: '100%'
            },
          ],
          strokeColor: 'transparent',
          strokeWidth: 0,
          map: map
        });
        meralcoCars[3] = new google.maps.Polyline({
          path: [
            {lat: 14.5874920, lng: 121.0638270},
            {lat: 14.5839230, lng: 121.0637530},
            {lat: 14.5818140, lng: 121.0637600},
            {lat: 14.5779470, lng: 121.0636180},
            {lat: 14.5749910, lng: 121.0610960},
            {lat: 14.5791610, lng: 121.0561540},
            {lat: 14.5834540, lng: 121.0511420},
            {lat: 14.5848550, lng: 121.0494690},
            {lat: 14.5871630, lng: 121.0465660}
          ],
          icons: [
            {
              icon: lineSymbol,
              offset: '100%'
            },
          ],
          strokeColor: 'transparent',
          strokeWidth: 0,
          map: map
        });

        // Associate the styled map with the MapTypeId and set it to display.
        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');

        google.maps.event.addDomListener(window, "resize", function() {
          var center = map.getCenter();
          google.maps.event.trigger(map, "resize");
          map.setCenter(center);
        });

        // Animation
        for (var i = 0; i < edsaCars.length; i++) {
          animateCars(edsaCars[i], i/8);
        }
        for (var i = 0; i < shawCars.length; i++) {
          animateCars(shawCars[i], i/6);
        }
        for (var i = 0; i < meralcoCars.length; i++) {
          animateCars(meralcoCars[i], i/8);
        }

        // Use the DOM setInterval() function to change the offset of the symbol
        // at fixed intervals.
        function animateCars(line, speed) {
          var count = 0;
          window.setInterval(function() {
            count = (count + 1) % 200;
            var icons = line.get('icons');
            icons[0].offset = (count / 2) + '%';
            line.set('icons', icons);
          }, (24 * (speed+1)));
        }
      });
    }
  });
});
