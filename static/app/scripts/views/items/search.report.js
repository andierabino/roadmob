define([
  'marionette',
  'views/items/report',
  'tpl!templates/items/search.report.tpl'
], function (Marionette, ReportView, tpl) {

  return ReportView.extend({
    template: tpl,
    ui: {
      shareFB: '.share-facebook',
      shareTwitter: '.share-twitter',
      shareKakao: '.share-kakao',
      reportImage: '.hero-card-image',
      reportInfo: '.hero-card-vehicle-info'
    },
    events: {
      'click @ui.shareFB': 'shareFB',
      'click @ui.shareTwitter': 'shareTwitter',
      'click @ui.shareKakao': 'shareKakao'
    },
    triggers: {
      "click @ui.reportImage": "report:clicked",
      "click @ui.reportInfo": "report:clicked"
    },
    onRender: function () {
      var self = this;
      // Get rid of that pesky wrapping-div.
      // Assumes 1 child element present in template.
      this.$el = this.$el.children();
      // Unwrap the element to prevent infinitely
      // nesting elements during re-render.
      this.$el.unwrap();
      this.setElement(this.$el);
      if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        self.ui.shareKakao.remove();
      }
    }
  });
});
