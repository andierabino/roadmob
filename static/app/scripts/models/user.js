define([
  'backbone',
  'vent'
], function (Backbone, vent) {
  var user = null;
  var UserModel = Backbone.Model.extend({
    urlRoot: '/api/user',
    defaults: {
      id: '',
      name: '',
      fb_id: '',
      email: '',
      contact: '',
      registered_at: ''
    },
    parse: function (data) {
      return data.user;
    },
    checkLoginState: function (callback) {
      var self = this;

      $.ajax({
        method: 'GET',
        url: '/api/login',
        success: function (response) {
          if (response.user){
            self.setup(response.user);

            if (typeof callback === "function") {
              callback(true);
            }
          } else {
            if (typeof callback === "function") {
              callback(response.user);
            }

          }

        },
        complete: function () {
          $("#user-menu").show();
        }

      });


    },
    fbLogin: function (callback) {
      var self = this;
      if (G.loadedFB) {
        FB.login(function(response) {
          if (response.status === 'connected') {
            var fbUserID = response.authResponse.userID;
            var accessToken = response.authResponse.accessToken;
            $.ajax({
              method: 'POST',
              url: '/api/login',
              data: {fb_id: fbUserID, access_token: accessToken},
              processData: true,
              success: function (response) {
                self.setup(response.user);
                if (typeof callback === "function") {
                  callback();
                }
              },
              complete: function () {
                $("#user-menu").show();
              }
            })
          } else if (response.status === 'not_authorized') {
            // The person is logged into Facebook, but not your app.
          } else {
            // The person is not logged into Facebook, so we're not sure if
            // they are logged into this app or not.
          }
        }, {scope: 'public_profile, email'});

      }
    },
    setup: function (user) {
      var self = this;
      self.set(user);
      G.me = user;
    },
    logout : function () {
      var self = this;
      $.ajax({
        method: 'GET',
        url: '/api/logout',
        success: function () {
          self.set(self.defaults);
          G.me = {};
          Backbone.history.navigate('', {trigger:true, replace: false});
        },
        complete: function () {
          $("#user-menu").show();
        }
      });
    }
  }, {
    getLoggedUser: function () {
      if (user==null) {
        user = new UserModel();
      }

      return user;
    }
  });

  return UserModel;
});
