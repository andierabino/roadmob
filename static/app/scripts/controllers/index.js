define([
  'app',
  'backbone',
  'commons/util'
], function (app, Backbone, util) {
  'use strict';
  var data;

  function load (path, params) {

    require(['views/layouts/' + path], function (PageView) {
      if (data) {
        params = _.extend({}, params, {
          data: data
        });
      }
      app.main.show(new PageView(params, data));
      data = null;

      ga('send', 'pageview', {
        page : '/' + Backbone.history.getFragment()
      });
    });
  }

  return {
    home: function () {
      load('home');
    },
    about: function () {
      load('about');
    },
    insights: function () {
      load('insights');
    },
    reportCreate: function () {
      load('reportCreate');
    },
    reportSearch: function (queryString) {
      var params = util.parseQueryString(queryString);
      load('reportSearch', params);
    },
    reportDetail: function (id) {
      load('reportDetail', {report_id: id});
    },
    dashboard: function () {
      load('dashboard');
    },
    setData : function(_data) {
      data = _data;
    }
  }
})
