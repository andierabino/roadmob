var PageModel = function(){
  (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id))
          return;
      js = d.createElement(s);
      js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js";
      fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  window.fbAsyncInit = function() {
      FB.init({
          appId: APP_ID,
          status: true,
          xfbml: true,
          cookie: true
      });
  };

  function getWindowWidth() {
    return Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
  }
  var self = this;
  var msnry;
  var grid;

  self.reports = ko.observableArray([]);
  self.hasMore = ko.observable(true);
  self.loading = ko.observable(true);
  self.searchQuery = ko.observable('');

  //get reports via ajax
  self.init = function(){
    $.ajax({
      method: 'GET',
      url: '/api/report?limit=4',
      dataType: 'json',
      complete: function () {
        self.loading(false);
      },
      success: function (result) {

        if (!result.has_more) {
          self.hasMore(false);
        }
        self.reports(ko.toJS(result.data));
        setTimeout(function () {
          grid = document.querySelector('#reports-collection');
          msnry = new Masonry( grid, {
            itemSelector: '.report-card',
            gutter: 5
          });
        },10)
      }

    });

  }
  self.loadMore = function (item, event) {

    if (!self.hasMore()) {
      $(event.target).remove();
      return;
    }
    var lastDate = self.reports().slice(-1)[0].created_at;
    var text = $(event.target).text()
    $.ajax({
      method: 'GET',
      url: '/api/report?limit=4&last='+lastDate,
      dataType: 'json',
      beforeSend: function(result) {
        $(event.target).html("").addClass('btn-loading');
      },
      success: function(result) {
        var data = ko.toJS(result.data);
        self.hasMore(result.has_more);
        for (var i in data) {
          self.reports.push(data[i]);
        }
        msnry.reloadItems();
        msnry.layout();
        var y = $(window).scrollTop() + 200;
        $('html,body').animate({scrollTop: y}, 300);
      },
      complete: function(result) {
        $(event.target).html(text).removeClass('btn-loading');
        if (!self.hasMore()) {
          $(event.target).remove();
        }
      }
    });
  }

  self.fbShare = function (report) {
    var link = REPORT_URI + report['id'];
    FB.ui({
      method: 'share',
      href: link
    });
  }

  self.twitterShare = function (report) {
    var link = "https://twitter.com/intent/tweet?text=" + report['plate_number'] + "+just+got+RoadMobbed!"
                + "&url=" + REPORT_URI + report['id'];
    var x = screen.width/2 - 700/2;
    var y = screen.height/2 - 450/2;
    window.open(encodeURI(link), "Roadmob Twitter Share", 'height=485,width=700,left='+x+',top='+y);
  }

  self.viewReport = function (report) {
    var link = '/report/' + report['id'];
    window.location = link;
  }

  self.search = function () {
    var query = self.searchQuery();
    window.location = '/search?q='+query;
  }

  self.searchOnKeyUp = function (item, event) {
    if (event.keyCode == 13)
      self.search();
  }
  self.init();
}

var pageModel = new PageModel();
ko.applyBindings(pageModel, document.getElementById("home-wrapper"));
