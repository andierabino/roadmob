var ReportModel = function () {
	var self = this;
	(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id))
          return;
      js = d.createElement(s);
      js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js";
      fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));



	window.fbAsyncInit = function() {
      FB.init({
          appId: APP_ID,
          status: true,
          xfbml: true,
          cookie: true
      });

			var finished_rendering = function() {
			  self.isCommentsLoaded(true)
			}

			// In your onload handler
			FB.Event.subscribe('xfbml.render', finished_rendering);
  };
	self.report = {}
	self.plate_number = ko.observable('');
	self.id = ko.observable('');
	self.vehicle_type = ko.observable('');
	self.operator = ko.observable('');
	self.incident = ko.observable('');
	self.date = ko.observable('');
	self.location = ko.observable('');
	self.created_at = ko.observable('');
	self.picture = ko.observable('');
	self.description = ko.observable('');
	self.isLoaded = ko.observable(false);
	self.isCommentsLoaded = ko.observable(false);
	self.init = function () {
		var self = this;
		var regex = /report\/(\w+)\//;
		var report_id = regex.exec(location.pathname)[1];
		$.ajax({
			method: 'GET',
			url: '/api/report/'+report_id,
			success: function (result) {
				ko.mapping.fromJS(result.data, {}, self);
			},
			complete: function () {
				self.isLoaded(true);
			}
		});


	}

	self.fbShare = function () {
		var link = REPORT_URI + self.id();
		console.log(link)
		FB.ui({
		  method: 'share',
		  href: link
		});
	}

	self.twitterShare = function (report) {
		var link = "https://twitter.com/intent/tweet?text=" + report['plate_number'] + "+just+got+RoadMobbed!"
		            + "&url=" + REPORT_URI+"/"+report['id'];
		var x = screen.width/2 - 700/2;
	    var y = screen.height/2 - 450/2;
	    window.open(encodeURI(link), "Roadmob Twitter Share", 'height=485,width=700,left='+x+',top='+y);
	}

	self.init();

}

//apply ko bindings to #reports-section
var reportModel = new ReportModel();
ko.applyBindings(reportModel, document.getElementById("report-detail-section"));
