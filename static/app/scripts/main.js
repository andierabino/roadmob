require.config({
  paths: {
    jquery: '../../bower_components/jquery/dist/jquery',
    underscore: '../../bower_components/underscore/underscore',
    backbone: '../../bower_components/backbone/backbone',
    "backbone.wreqr" : "../../bower_components/backbone.wreqr/lib/backbone.wreqr",
    marionette: '../../bower_components/backbone.marionette/lib/backbone.marionette',
    tpl: '../../bower_components/requirejs-tpl/tpl',
    masonry: '../../bower_components/masonry/dist/masonry.pkgd',
    imagesloaded: '../../bower_components/imagesloaded/imagesloaded.pkgd',
    clipboard: '../../bower_components/clipboard/dist/clipboard',
    facebook : '//connect.facebook.net/en_US/sdk',
    ga : '//www.google-analytics.com/analytics',
    googlemaps: '../../bower_components/googlemaps-amd/src/googlemaps',
    async: '../../bower_components/requirejs-plugins/src/async',
    api : 'commons/api',
    router : 'routers/index',
    controller : 'controllers/index',
    routerFilter: '../../bower_components/backbone-route-filter/backbone-route-filter',
    datePicker: '../../vendor/datepicker/js/bootstrap-datepicker',
    wookmark: '../../bower_components/wookmark/wookmark',
    randomcolor: '../../bower_components/randomcolor/randomColor',
    kakao: '../../vendor/kakao.min'
  },
  waitSeconds: 0,
  shim: {
    jquery: {
      exports: '$'
    },
    facebook: {
      exports: 'FB'
    },
    kakao: {
      exports: 'Kakao'
    },
    "ga": {
        exports: "__ga__"
    },
    wookmark: ['jquery']
  },
  googlemaps: {
    params: {
      key: 'AIzaSyDoPvS1Sz4yPebwOltD9IxyLU8Olstomrw',
      libraries: 'places'
    }
  }
});

define([
  'app',
  'commons/api',
  'router',
  'vent'
], function (app, api, router, vent) {
  window.ga = function() {};
  window.G = {
    loadedFB: false
  }
  window.router = new router();
  api.init();
  app.start();

  require(['kakao'], function (Kakao) {
    Kakao.init('836ad906abdd52d3f81c1b1c49786385');
  });

  require(['ga'], function() {
    if (location.host == 'www.roadmob.ph') {
      ga('create','UA-71595705-1', 'auto');
    }
  });

  require(['facebook'], function (FB) {
    FB.init({
      appId: FB_APP_ID,
      xfbml: true,
      version: 'v2.4'
    });
    G.loadedFB = true;
    FB.Event.subscribe('xfbml.render', function () {
      $('.comments-loading').remove();
    });

  });
});
