<div class="grid-container padded-bottom-10">
  <div class="dashboard-region no-heading text-center v-padded-15">
    <span class="text-large"><b>My Reports</b></span>
  </div>
  <div class="grid-100 grid-parent">

    <div class="grid-100 grid-parent">

      <div class="no-reports text-medium text-center v-padded-25" style="display:none">No reports to display.</div>
      <div id="dashboard-reports-collection" class="grid-100 grid-parent">

      </div>
    </div>

    <div class="see-more text-center grid-100 v-padded-25" style="display:none">
      <a class="btn btn-default btn-small btn-medium">See More</a>
    </div>
  </div>
</div>
