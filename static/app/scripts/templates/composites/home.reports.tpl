<div class="grid-90 prefix-5">
  <div class="grid-60 tablet-grid-60">
    <h6 class="section-header">People have been reporting</h6>
  </div>
  <!--
  <div class="grid-40 tablet-grid-40 text-right hide-on-mobile">
  TODO: Get count of reports this week! 
    <h6 class="section-header text-meta"><span class="text-meta count_span"><%= total %></span> report<% (total > 1) ? 's' : ''%> this week</h6>
  -->
  </div>
</div>
<div class="grid-90 prefix-5 grid-parent">
  <div class="text-center">
    <div class="reports-loading text-center v-padded-25">
      <div class="preloader">
          <span></span>
          <span></span>
      </div>
    </div>
  </div>

  <div id="reports-collection" class="grid grid-100 grid-parent opaque">

  </div>

  <div class="text-center grid-100 v-padded-25">
    <a class="see-more btn btn-normal btn-medium" style="display:none">See More</a>
  </div>
</div>
