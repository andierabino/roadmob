<div class="grid-container max-10000">
  <div class="text-center heading-medium form-header grid-100 v-padded-50 grid-parent">File a Report</div>
</div>
<div class="grid-container">

  <div id="report-detail-section" class="grid-50 prefix-25 tablet-grid-90 tablet-prefix-5 mobile-grid-90 mobile-prefix-5 v-padded-50">

      <div class="form grid-100 grid-parent">

        <div class="form-body">
          <div class="heading-medium mobile-grid-100">1. Report Details</div>
          <div class="mobile-grid-100 text-meta v-padded-15">
             Please ensure that details you provide in this report are true. We reserve the right to take down false and nuisance reports. Thank you for taking this initiative of making our commute better and safer.
          </div>
          <div class="input-group grid-100 tablet-grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
              Vehicle Type <span class="text-red">*</span>:
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">
              <select id="vehicle_type" class="grid-100 tablet-grid-100 mobile-grid-100" name="vehicle_type">
                <option value="" selected>Select a type of vehicle</option>
                <option value="Bus">Bus</option>
                <option value="GrabCar">GrabCar</option>
                <option value="Jeepney">Jeepney</option>
                <option value="Motorcycle">Tricycle</option>
                <option value="School Service">School Service</option>
                <option value="Shuttle Service">Shuttle Service</option>
                <option value="Taxi">Taxi</option>
                <option value="Tourist Bus">Tourist Bus</option>
                <option value="Tourist Car">Tourist Car</option>
                <option value="Truck for Hire">Truck for Hire</option>
                <option value="Uber">Uber</option>
                <option value="UV Express">UV Express</option>
              </select>
            </div>
          </div>

          <div class="input-group grid-100 tablet-grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
              Plate Number <span class="text-red">*</span>:
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">
              <input id="plate_number" class="grid-100 tablet-grid-100 mobile-grid-100" type="text" name="plate_number" placeholder="Example: ABC-123"/>
            </div>
          </div>

          <div class="input-group grid-100 tablet-grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
            Operator Name:
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">
              <input id="operator" class="grid-100 tablet-grid-100 mobile-grid-100" type="text" name="operator"/>
            </div>
          </div>

          <div class="input-group grid-100 tablet-grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
              Type of Complaint <span class="text-red">*</span>:
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">
              <select id="complaint_type" class="grid-100 tablet-grid-100 mobile-grid-100" name="complaint_type">

                <option value="" selected>Select a type of incident</option>
                <option value="Arrogant/Discourteous Driver">Arrogant/Discourteous Driver</option>
                <option value="Contracting Passenger">Contracting passenger</option>
                <option value="Defective Meter">Defective Meter</option>
                <option value="Discriminating Against Passenger">Discriminating against passenger</option>
                <option value="Fast Meter">Fast Meter</option>
                <option value="Hit and Run">Hit and Run</option>
                <option value="Reckless Driving">Reckless Driving</option>
                <option value="Refusal to Convey passenger">Refusal to Convey passenger</option>
                <option value="Refusal to Grant Discount">Refusal to grant discount</option>
                <option value="Threatening  Passenger">Threatening  Passenger</option>
                <option value="No Flag Down Meter">No Flag Down meter</option>
                <option value="Out of Line">Out of Line</option>
                <option value="Overcharging/Undercharging">Overcharging/Undercharging</option>
                <option value="Smoke Belching">Smoke Belching</option>
                <option value="Others">Others</option>

              </select>
            </div>
          </div>

          <div class="input-group grid-100 tablet-grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
              Date <span class="text-red">*</span>:
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">
              <input class="grid-100" type="text" id="date" name="date" placeholder="When did it happen?"/>
            </div>
          </div>

          <div class="input-group grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
              Location <span class="text-red">*</span>:
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">
              <div class="grid-100 grid-parent" id="locationField">
                <input class="grid-100 tablet-grid-100 mobile-grid-100" type="text" id="location" name="location" placeholder="Where did the incident happen?"/>
              </div>
            </div>
          </div>

          <div class="input-group grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
              Complaint <span class="text-red">*</span>:
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">
              <textarea id="description" rows="5"class="grid-100 tablet-grid-100 mobile-grid-100" type="text" name="description" placeholder="Please tell us more about what happened."></textarea>
            </div>
          </div>

           <div class="input-group grid-100 tablet-grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
              Upload Photo
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">
              <input name="picture" type="file" value="Select File" accept='image/<span class="text-red">*</span>'/>
              <div class="upload-preview-wrapper">
              <a class="clear-upload hidden"><i class="fa fa-close"></i></a>
              <img id="upload-preview">
              </div>
            </div>
          </div>

        </div>

        <!--Start of Contact Info-->
        <div class="form-body padded-bottom-20">
          <div class="heading-medium mobile-grid-100">2. Contact Info</div>
          <div class="mobile-grid-100 text-meta v-padded-15">
            We don't share this information with other users. We need this in case authorities need to get in touch with you in process of addressing your complaint.
          </div>
          <div class="input-group grid-100 tablet-grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
              Contact Number <span class="text-red">*</span>:
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">

              <input id="contact" class="grid-100 tablet-grid-100 mobile-grid-100" type="number" name="contact"/>

            </div>
          </div>
          <div class="input-group grid-100 tablet-grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
              Email Address <span class="text-red">*</span>:
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">
              <input id="email" class="grid-100 tablet-grid-100 mobile-grid-100" type="email" name="email"/>

            </div>
          </div>
      </div>
      <!--End of Contact Info-->


  </div>
  <div class="grid-100 grid-parent">
    <div class="grid-100 grid-parent input-group">
      <a class="btn-medium btn btn-default link" href="/">Cancel</a>
      <a class="btn-medium btn btn-green" id="submit-report">Submit</a>
  </div>
</div>
