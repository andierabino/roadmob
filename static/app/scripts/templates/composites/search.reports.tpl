<div class="grid-container max-10000 v-padded-50 page-header">
  <div class="grid-container">
    <div class="grid-90 prefix-5">
	    <div class="text-large text-center"><b>Search Reports</b></div>
			<div class="grid-100 text-center grid-parent text-small">
				<b id="search-results-text"></b>
			</div>
	  </div>
  </div>
</div>
<div class="grid-container v-padded-25" id="report-search">
	<div class="grid-90 prefix-5">
		<div id="reports-collection" class="grid grid-100 grid-parent">
		</div>
	</div>
</div>
