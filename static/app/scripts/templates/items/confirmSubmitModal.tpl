<a class="modal-close"><i class="fa fa-times"></i></a>
<div class="grid-container">
  <p class="v-padded-15 text-left">Please ensure that details you provided in this report are true as this will be forwarded to LTFRB. We reserve the right to take down false and nuisance reports. Thank you for taking this initiative of making our commute better and safer.</p>
	<div class="grid-100 text-right">

    <a id="confirm" class="btn btn-normal-inverse">Confirm</a>
    <a id="cancel" class="btn-red btn">Cancel</a>
	</div>
</div>
