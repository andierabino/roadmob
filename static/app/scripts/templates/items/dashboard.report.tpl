<div class="hero-card">
  <div class="hero-card-inner">
    <% if (picture) { %>
    <div class="hero-card-image">
      <div class="image" style="background-image:url('http://<%= picture %>');"></div>
      <div class="hero-card-overlay overlay-top">
        <a class="share-button share-facebook facebook"><i class="fa fa-facebook"></i></a>
        <a class="share-button share-twitter twitter"><i class="fa fa-twitter"></i></a>
      </div>
    </div>
    <% } else { %>
    <div class="hero-card-image">
      <div class="image" style="background-image: url('https://maps.googleapis.com/maps/api/staticmap?center=<%=location%>&zoom=14&size=400x240&key=AIzaSyDoPvS1Sz4yPebwOltD9IxyLU8Olstomrw')"></div>
      <div class="hero-card-overlay overlay-top">
        <a class="share-button share-facebook facebook"><i class="fa fa-facebook"></i></a>
        <a class="share-button share-twitter twitter"><i class="fa fa-twitter"></i></a>
      </div>
    </div>

    <% } %>
    <div class="hero-card-vehicle">
      <div class="hero-card-vehicle-icon icon-yellow motorcycle" style="background-image:url('/static/app/images/vehicles/<%= vehicle_type.toLowerCase().split(' ').join('_') %>.svg')"></div>
      <div class="hero-card-vehicle-info">
        <h5><%= plate_number %></h5>
        <h6><%= vehicle_type %></h6>
      </div>
    </div>
    <div class="hero-card-reporttype">
      <h5><%= incident %></h5>
    </div>
    <div class="hero-card-details">
      <p class="location"><i class="fa fa-map-marker"></i> <%= location %></p>
      <p class="description"><%= description %></p>
    </div>
    <div class="hero-card-postinfo">
      <small>Reported on <%= date_text %></small>
    </div>
  </div>
</div>
