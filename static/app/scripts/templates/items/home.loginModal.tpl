<a class="modal-close"><i class="fa fa-times"></i></a>
<div class="grid-container">
	<div class="grid-90 prefix-5 text-center">
    <img src="/static/app/images/favicon.png" width="50px">
    <p class="heading-medium">Login to Roadmob</p>
		<a style="margin-top: 50px;" class="modal-fb-login btn btn-normal-inverse btn-block" data-next="file-report"><i class="fa fa-facebook"></i> Log in with Facebook</a>
	</div>
</div>
