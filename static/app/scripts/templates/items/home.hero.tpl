

<div class="hero-map" ></div>
<div class="hero-overlay">
  <div class="hero-overlay__container">
    <div class="grid-90 prefix-5">
        <div class="grid-75 tablet-grid-60">
          <div class="mobile-grid-100">
            <h5>Make commuting in the Philippines safer and better.</h5>
            <h1>Join the mob against abusive drivers and operators</h1>
            <a href="/report/new" class="link btn btn-large" id="file-main">File a report</a>
          </div>
        </div>
        <div class="grid-25 tablet-grid-40 hide-on-mobile">
          <div class="mobile-grid-100 tablet-grid-90 tablet-prefix-5 grid-parent">
            <div class="hero-card" style="padding-top: 0;">
              <div class="hero-card-inner">
                <div class="hero-card-image">
                  <div class="image" style="background-image:url('/static/app/images/city-traffic-people-smartphone.jpg');"></div>
                  <div class="hero-card-overlay overlay-top">
                    <a class="share-button facebook"><i class="fa fa-facebook"></i></a>
                    <a class="share-button twitter"><i class="fa fa-twitter"></i></a>
                  </div>
                </div>
                <div class="hero-card-vehicle">
                  <div class="hero-card-vehicle-icon icon-yellow motorcycle" style="background-image:url('/static/app/images/vehicles/taxi.svg')"></div>
                  <div class="hero-card-vehicle-info">
                    <h5>ABC-123</h5>
                    <h6>Taxi</h6>
                  </div>
                </div>
                <div class="hero-card-reporttype">
                  <h5>Fast Meter</h5>
                </div>
                <div class="hero-card-details">
                  <p class="location"><i class="fa fa-map-marker"></i> Makati City, Philippines</p>
                  <p class="description">I noticed that the charge on the taxi's meter started going...</p>
                </div>
                <div class="hero-card-postinfo">
                  <small> Reported on December 10, 2015 12:30 PM</small>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
