<div id="about" class="grid-container v-padded-36">
  <div class="grid-100 tablet-grid-100 mobile-grid-100">
    <div class="grid-90 prefix-5">
      <h6 class="section-header text-center">How Roadmob works</h6>
      <p class="section-subheader text-center">Roadmob is an online platform for reporting abusive PUV drivers and operators. <br>With Roadmob, we can collectively make our commute experience safer and better. </p>
    </div>
  </div>
  <div class="how-it-works-container grid-100 tablet-grid-100 mobile-grid-100">
    <div class="grid-90 prefix-5">
      <div class="how-it-works-entry text-center grid-33 tablet-grid-100 mobile-grid-100">
        <div class="how-it-works-icon" style="background-image: url('/static/app/images/about/file-report.svg')">
        </div>
        <h5>File a report</h5>
        <p>Commuters can publish a complaint against abusive PUV drivers and operators. These complaints are accessible to the public and are forwarded to LTFRB and other concerned parties for appropriate action.</p>
      </div>
      <div class="how-it-works-entry text-center grid-33 tablet-grid-100 mobile-grid-100">
        <div class="how-it-works-icon" style="background-image: url('/static/app/images/about/join-mob.svg')">
        </div>
        <h5>Join the mob</h5>
        <p>Published reports can be shared on Facebook and Twitter. Each report includes a forum to facilitate a conversation between commuters and concerned parties.</p>
      </div>
      <div class="how-it-works-entry text-center grid-33 tablet-grid-100 mobile-grid-100">
        <div class="how-it-works-icon" style="background-image: url('/static/app/images/about/drive-policy.svg')">
        </div>
        <h5>Drive policy and decision-making</h5>
        <p>Reports from proactive commuters are the key ingredients towards generating insights that can help authorities like LTFRB and MMDA develop new policies and impose the existing ones on violators.</p>
      </div>
    </div>
  </div>
  <div class="grid-100 tablet-grid-100 mobile-grid-100">
    <div class="grid-90 prefix-5 padded-top-50">
      <h6 class="section-header text-center">The Team</h6>
      <p class="grid-80 prefix-10 section-subheader text-center">Roadmob was originally developed by Andie, Ting, Jason, Ansley, and Franco during a hackathon in the UP Department or Computer Science last April 2015. It will be featured during the 1st <a href="//hackfair.gdgph.org">GDG PH Hackfair</a> in Mind Museum on December 18-19, 2015. 
      <br><br>
      Keep in touch with the team through our social media channels. <br>
      Facebook: fb.com/roadmobph <br>
      Twitter:  @roadmobph <br> </p>
    </div>
  </div>
</div>
