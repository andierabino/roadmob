<div class="grid-container v-padded-36">
  <div class="grid-100 tablet-grid-100 mobile-grid-100">
    <div class="grid-90 prefix-5">
      <h6 class="section-header">People have been reporting</h6>
      <div class="live-reports-carousel">
        <div class="live-reports">
          <% var vehicles = ['motorcycle', 'car', 'uv']; %>
          <% var reportTypes = ['Discriminating against passenger', 'Overcharging', 'Rude driver', 'Swerving']; %>
          <% var plateNumbers = ['ABC-123', 'XYZ-245', 'MA-45233', 'MT-23412']; %>
          <% var locations = ['Buendia,Makati', 'UP Diliman,Quezon City', 'P.Tuazon,EDSA', 'BGC,Taguig', 'C-5 Road,Taguig', 'SM Megamall,Ortigas', 'Jollibee Guadalupe,Makati', 'Robinsons,Ortigas']; %>
          <% for ( var i = 0; i < 8; i++ ) { %>
            <div class="live-report-wrapper">
              <div class="live-report">
                <div class="live-report-image" style="background-image:url('https://maps.googleapis.com/maps/api/staticmap?center=<%=locations[i]%>&zoom=14&size=121x121&key=AIzaSyDz4bGthVRfyp9VvUoi-uG-ubTJDE8ffAU');">
                  <div class="live-report-overlay <%= vehicles[Math.round(Math.random() * (vehicles.length-1))] %>" style="background-color:rgba(255,255,255,0.22);"></div>
                </div>
                <div class="live-report-reporttype">
                  <h5><%= reportTypes[Math.round(Math.random() * (reportTypes.length-1))] %></h5>
                </div>
                <div class="live-report-plate">
                  <h6><%= plateNumbers[Math.round(Math.random() * (plateNumbers.length-1))] %></h6>
                </div>
              </div>
            </div>
          <% } %>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- <div>
  var colors = randomColor({hue: 'monochrome', count: 8, format: 'rgb'}); //colors[i].replace(')', ', 0.25)').replace('rgb', 'rgba')
</div> -->
