<div class="grid-100 text-left">

	<div class="report-detail grid-60 prefix-20 tablet-grid-80 tablet-prefix-10 mobile-grid-90 mobile-prefix-5">

		<div class="report-detail-inner grid-100 grid-parent">
			<a class="modal-close"><i class="fa fa-times"></i></a>
      <% if (picture) { %>
      <div class="hero-card-image">
        <div class="image" style="background-image:url('http://<%= picture %>');"></div>

      </div>
      <% } else { %>
      <div class="hero-card-image">
        <div class="image" style="background-image: url('https://maps.googleapis.com/maps/api/staticmap?center=<%=location%>&zoom=14&size=800x480&key=AIzaSyDoPvS1Sz4yPebwOltD9IxyLU8Olstomrw')"></div>

      </div>
      <% } %>
      <div class="report-detail-inner-section grid-100 grid-parent">
  			<div class="text-medium"><b><%= plate_number %></b></div>
        <div class="heading-meta"><%= vehicle_type %></div>
      </div>

      <div class="report-detail-inner-section grid-100 grid-parent">
        <div class="report-incident"><%= incident %></div>
      </div>
      <div class="report-detail-inner-section grid-100 grid-parent">
        <p class="text-meta"><i class="fa fa-map-marker"></i> <%= location %></p>
  			<p ><%= description %></p>
      </div>

      <div class="report-detail-inner-section grid-100 grid-parent">
			     <p class="text-meta">Posted on <%= date_text %></p>
      </div>

			<div class="report-detail-inner-section grid-100 grid-parent">
				<a class="share-facebook btn btn-social btn-default-inverse btn-circle">
					<i class="text-small fa fa-facebook"></i></a>
				<a class="share-twitter btn-social btn btn-tweet btn-default-inverse btn-circle">
					<i class="text-small fa fa-twitter"></i>
				</a>
        <a class="share-kakao btn-social btn btn-kakao btn-circle">
				</a>
			</div>

		</div>
		<div class="report-detail-inner report-comments">
      <div class="report-detail-inner-section">
  			<div class="text-medium v-padded-15"><b>Comments</b></div>
  			<div class="text-center v-padded-15 comments-loading">
          <div class="preloader">
              <span></span>
              <span></span>
          </div>
        </div>
  			<div class="grid-100 grid-parent">
  				<div class="fb-comments" data-href="<%= REPORT_URI + id %>" data-numposts="10" data-width="100%">
  				</div>
  			</div>
      </div>
		</div>
	</div>
</div>
