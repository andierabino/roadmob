<div id="banner-section">
  <div class="grid-container max-10000 banner-container">
    <div class="text-center banner-inner">
      <div class="banner-contents">
        <div class="text-medium">
          We aim to generate insights from data on roadmob to drive policy and decision-making in transportation. We do not have enough data to deliver these insights as of now.

          <br><br>Help us get there by sharing Roadmob to your social circle.
        </div>
        <br>
        <a class="link btn btn-social btn-facebook btn-circle"><i class="fa fa-facebook"></i></a>
        <a class="link btn btn-social btn-tweet btn-circle"><i class="fa fa-twitter"></i></a>
      </div>
      <div class="building">
        <div class="vehicle">
          <img class="jeep-moving" src="/static/app/images/jeep.png" width="50">
          <img class="car-moving" src="/static/app/images/bus.png" width="60">
          <img class="taxi-moving" src="/static/app/images/car.png" width="40">
        </div>
      </div>
      <div class="white-overlay"></div>
    </div>
  </div>
</div>
