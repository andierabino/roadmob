<div id="hero-region" class="grid-container max-10000"></div>
<div id="live-reports-region" class="grid-container max-10000"></div>

<!-- <div id="banner-section">
  <div class="grid-container max-10000">
    <div class="grid-50 prefix-25 text-center">
      <div class="banner-contents">
        <div class="tagline">
          Help fellow commuters.<br>
          Report irresponsible drivers.
        </div>
        <br>
        <a href="/report/new" class="link btn btn-large" id="file-main">File A Report</a>
      </div>
      <div class="building">
        <div class="vehicle">
          <img class="jeep-moving" src="/static/app/images/jeep.png" width="50">
          <img class="car-moving" src="/static/app/images/bus.png" width="60">
          <img class="taxi-moving" src="/static/app/images/car.png" width="40">
        </div>
      </div>
      <div class="white-overlay"></div>
    </div>
  </div>
</div> -->

<div id="report-collection-region" class="grid-container v-padded-36"></div>
<div id="how-it-works-region" class="grid-container max-10000"></div>

<div id="modal-overlay" class="modal-region"></div>

<div id="report-modal-region" class="modal-region hidden">

</div>

<div class="footer-notice text-center">
  <a id="close-notice" class="modal-close"><i class="fa fa-times"></i></a>
  Roadmob is currently in its beta phase. You can write us your feedback by clicking <a target="_blank" href="//bit.ly/roadmobph-feedback">here</a>.
</div>
