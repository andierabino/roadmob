# -*- coding: utf-8 -*-
from rest_framework import serializers

from .models import Report


class ReportSerializer(serializers.ModelSerializer):

    date_text = serializers.CharField(read_only=True, source='datetext')
    description = serializers.CharField(required=False, allow_blank=True)
    operator = serializers.CharField(required=False, allow_blank=True)
    class Meta:
        model = Report
        exclude = ('user',)

    def __init__(self, *args, **kwargs):
        enable_user = kwargs.pop('enable_user', True) # False is the default
        super(ReportSerializer, self).__init__(*args, **kwargs)
        if enable_user:
            self.Meta.exclude = tuple(x for x in self.Meta.exclude if x != 'user')

    def create(self, validated_data):
        """
        Create and return a new `Report` instance, given the validated data.
        """
        return Report.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.plate_number = validated_data.get('plate_number',instance.plate_number)
        instance.vehicle_type = validated_data.get('vehicle_type',instance.vehicle_type)
        instance.operator = validated_data.get('operator')
        instance.incident = validated_data.get('incident')
        instance.date = validated_data.get('date')
        instance.location = validated_data.get('location')
        instance.created_at = validated_data.get('created_at')
        instance.updated_at = validated_data.get('updated_at')
        instance.deleted_at = validated_data.get('deleted_at')
        instance.picture = validated_data.get('picture')
        instance.description = validated_data.get('description')
        instance.save()

        return instance
