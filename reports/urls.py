# -*- coding: utf-8 -*-
from django.conf.urls import url

from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework import routers

from .api import ReportViewSet
from .views import (
    create as ReportCreateView,
    search as ReportSearchView,
    ReportDetailView,
    dashboard as DashboardView,
    about as AboutView,
    insights as InsightsView,
)

router = routers.SimpleRouter()
router.register(r'^api/report', ReportViewSet)

slashlessRouter = routers.SimpleRouter(trailing_slash=False)
slashlessRouter.register(r'^api/report', ReportViewSet)

urlpatterns = [
    url(r'^report/new$', ReportCreateView),
    url(r'^report/(?P<pk>\d+)$', ReportDetailView.as_view(), name='detail'),
    url(r'^search/', ReportSearchView),
    url(r'^dashboard$', DashboardView),
    url(r'^dashboard/$', DashboardView),
    url(r'^about$', AboutView),
    url(r'^insights$', InsightsView),
]

urlpatterns += router.urls
urlpatterns += slashlessRouter.urls
