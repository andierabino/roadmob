from django.db import models
from django.forms import ModelForm
from django.core.validators import RegexValidator

from users.models import User

class PlateNumberValidator(RegexValidator):
    regex = r'^[A-za-z]{2,3}\-*[0-9]{3,5}$'
    message = 'Invalid plate number.'


class Report(models.Model):

    user = models.ForeignKey(User, null=False)
    plate_number = models.CharField(max_length=8,db_index=True,null=False, validators=[PlateNumberValidator()])
    vehicle_type = models.CharField(max_length=20,null=False)
    operator = models.CharField(max_length=32,blank=True,null=True)
    route = models.CharField(max_length=100,blank=True,null=True)
    incident = models.CharField(max_length=128, null=False)
    date = models.DateTimeField(null=False)
    location = models.CharField(max_length=256,null=False)
    created_at = models.DateTimeField(auto_now_add=True,null=True)
    updated_at = models.DateTimeField(auto_now=True,null=True)
    deleted_at = models.DateTimeField(null=True)
    picture = models.CharField(max_length=128, blank=True, null=True)
    description = models.CharField(max_length=5000,null=True)

    def datetext(self):
        return self.date.strftime("%B %d, %Y %I:%M %p")
