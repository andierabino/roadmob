from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope
from rest_framework import permissions

class IsAuthenticatedOrToken(TokenHasReadWriteScope):
	def has_permission(self, request, view):
		print(request.user)
		if request.method == "POST" and (not request.user or not request.user.is_authenticated()):
			return super(self.__class__, self).has_permission(request, view)
		elif request.method == "POST" and request.user.is_authenticated():
			return True
		elif request.method in permissions.SAFE_METHODS:
			return True
		return False
        # elif not request.user.is_authenticated() and request.method != "POST":
        #     return False
        # elif request.method in permissions.SAFE_METHODS:
        #     return True