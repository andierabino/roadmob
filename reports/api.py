# -*- coding: utf-8 -*-
import os
import hashlib
import json

from roadmob import utils
from datetime import datetime

from django.db.models import Q
from django.conf import settings

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from rest_framework import filters
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes, list_route
from rest_framework.parsers import JSONParser
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from dateutil.parser import parse

from .serializers import ReportSerializer
from .models import Report
from users.models import User

import boto3
import uuid

from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope, OAuth2Authentication
from .custom_permissions import IsAuthenticatedOrToken

class ReportViewSet(viewsets.ViewSet):
    serializer_class = ReportSerializer
    queryset = Report.objects.extra({'date':"date at time zone 'Asia/Taipei'"}).all().order_by('-created_at')
    authentication_classes = [OAuth2Authentication, SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticatedOrToken]

    #retrieve all results
    def list(self, request):
        #print("user ", request.user)
        #print("user auth ", request.user.is_authenticated())
        has_more = False
        params = request.GET.copy()
        filters = {
            'deleted_at' : None
        }
        excludes = {}
        if 'location' in params:
            filters['location__icontains'] = params['location']
        if 'plate_number' in params:
            filters['plate_number__icontains'] = params['plate_number']
        if 'operator' in params:
            filters['operator__icontains'] = params['operator']
        if 'data' in params:
            filters['date'] = params['date']
        if 'last' in params:
            filters['created_at__lt'] = parse(params['last'])

        if 'user' in params:
            if params['user'] == 'others':
                excludes['user'] = request.user
            else:
                filters['user'] = request.user
        if 'q' in params:
            search_query = request.GET.get('q')
            filters = Q(plate_number__icontains=search_query) | \
                        Q(description__icontains=search_query) | \
                        Q(location__icontains=search_query) | \
                        Q(incident__icontains=search_query)

            queryset = Report.objects.filter(filters,deleted_at=None).distinct('id')
        else:
            queryset = Report.objects.filter(**filters) \
                    .order_by('-created_at').extra({'date':"date at time zone 'Asia/Taipei'"}).exclude(**excludes).all()


        if 'page' in params and 'limit' in params:
            pageNumber = int(params['page'])
            limit = int(params['limit'])
            start = pageNumber*limit
            has_more = queryset[start+limit:].count() > 0
            queryset = queryset[start:start+limit]

        elif 'limit' in params:
            has_more = queryset.count() > int(params['limit'])
            queryset = queryset[:int(params['limit'])]

        serializer = ReportSerializer(queryset,many=True)
        return Response({"data": serializer.data, 'has_more': has_more })


    #create instance of the model
    def create(self, request): #implement this
        data = request.data
        report = json.loads(data['report'])

        if request.user != None:
            # Coming from web
            report['user'] = request.user.id
        else:
            # Coming from app
            user_id = report['user']
            report['user'] = User.objects.get(user_id=uuid.UUID(user_id)).id

        userinfo = json.loads(data['userinfo'])
        user = User.objects.get(pk=report['user'])
        user.contact = userinfo['contact']
        user.save()

        botoclient = boto3.client('s3',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)

        serializer = ReportSerializer(data=report)
        if serializer.is_valid():
            new_report = serializer.save()
            new_report = Report.objects.get(id=new_report.id)
            #upload picture
            if 'picture' in request.FILES:
                picture = request.FILES['picture']
                upload_timestamp = int(datetime.now().strftime("%s")) * 1000
                filename ='image/report/{}/{}.jpg'.format(
                    new_report.id,
                    upload_timestamp
                )

                r = botoclient.put_object(ACL='public-read',
                              Body=picture.read(),
                              ContentType=picture.content_type,
                              Key=filename,
                              Bucket='christinebkt1')

                status_code = r['ResponseMetadata']['HTTPStatusCode']

                if status_code == 200:
                    url = '{}/{}'.format(settings.AWS_S3_CUSTOM_DOMAIN, filename)
                    new_report.picture = url
                    new_report.save()

            serializer = ReportSerializer(new_report,many=False, enable_user=False)

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #get data from database
    def retrieve(self, request, pk=None):
        serializer = ReportSerializer(self.queryset.get(id=pk),many=False, enable_user=False)
        return Response({"data":serializer.data})

    #modify entry in database - all fields
    def update(self, request, pk=None):
        report = Report.objects.get(pk=pk)
        serializer = ReportSerializer(report, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"data":serializer.data})

    #modify at least 1 field
    def partial_update(self, request, pk=None):
        pass

    #remove from database
    def destroy(self, request, pk=None):
        report = Report.objects.get(pk=pk)
        report.deleted_at=datetime.utcnow()
        report.save()
        return Response({})

    #search report
    @list_route()
    def search(self, request):
        search_query = request.GET.get('q')
        filters = Q(plate_number__icontains=search_query) | \
                    Q(description__icontains=search_query) | \
                    Q(location__icontains=search_query) | \
                    Q(incident__icontains=search_query)

        queryset = Report.objects.filter(filters,deleted_at=None).distinct('id')
        serializer = ReportSerializer(queryset,many=True)
        return Response({"data": serializer.data})


#for ajax calls, for get/sending data
