# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0005_auto_20151217_1435'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='description',
            field=models.CharField(null=True, max_length=5000),
        ),
    ]
