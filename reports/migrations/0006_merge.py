# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0005_auto_20151217_1435'),
        ('reports', '0005_merge'),
    ]

    operations = [
    ]
