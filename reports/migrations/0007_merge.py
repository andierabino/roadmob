# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0006_auto_20151218_1601'),
        ('reports', '0006_merge'),
    ]

    operations = [
    ]
