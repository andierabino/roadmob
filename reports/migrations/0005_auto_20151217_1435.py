# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0004_auto_20151212_1017'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='vehicle_type',
            field=models.CharField(max_length=20, choices=[('Private Vehicle', 'Private Vehicle'), ('Shuttle Service', 'Shuttle Service'), ('Taxi', 'Taxi'), ('Bus', 'Bus'), ('Jeepney', 'Jeepney'), ('Motorcycle', 'Motorcycle'), ('UV Express', 'UV Express'), ('School Service', 'School Service'), ('Truck for Hire', 'Truck for Hire'), ('Tourist Bus', 'Tourist Bus'), ('Tourist Car', 'Tourist Car'), ('Uber', 'Uber'), ('GrabCar', 'Grabcar')]),
        ),
    ]
