# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='vehicle_type',
            field=models.CharField(max_length=20, choices=[('Shuttle Service', 'Shuttle Service'), ('Taxi', 'Taxi'), ('Bus', 'Bus'), ('Jeepney', 'Jeepney'), ('UV Express', 'UV Express'), ('School Service', 'School Service'), ('Truck for Hire', 'Truck for Hire'), ('Tourist Bus', 'Tourist Bus'), ('Tourist Car', 'Tourist Car'), ('Uber', 'Uber'), ('GrabCar', 'Grabcar')]),
        ),
    ]
