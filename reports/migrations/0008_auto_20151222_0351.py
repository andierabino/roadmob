# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0007_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='incident',
            field=models.CharField(max_length=128),
        ),
        migrations.AlterField(
            model_name='report',
            name='vehicle_type',
            field=models.CharField(max_length=20),
        ),
    ]
