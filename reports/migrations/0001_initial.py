# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import reports.models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Report',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('plate_number', models.CharField(max_length=8, db_index=True, validators=[reports.models.PlateNumberValidator()])),
                ('vehicle_type', models.CharField(max_length=20, choices=[('Jeepney', 'Jeepney'), ('Bus', 'Bus'), ('Taxi', 'Taxi'), ('Sedan', 'Sedan'), ('SUV', 'SUV'), ('Motorbike', 'Motorbike'), ('Others', 'Others')])),
                ('operator', models.CharField(max_length=32, null=True, blank=True)),
                ('route', models.CharField(max_length=100, null=True, blank=True)),
                ('incident', models.CharField(max_length=128, choices=[('Refusal to Convey passenger', 'Refusal to Convey passenger'), ('Fast Meter', 'Fast Meter'), ('Contracting passenger', 'Contracting passenger'), ('Defective Meter', 'Defective Meter'), ('Arrogant/Discourteous Driver', 'Arrogant/Discourteous Driver'), ('Overcharging/Undercharging', 'Overcharging/Undercharging'), ('No Flag Down meter', 'No Flag Down meter'), ('Out of Line', 'Out of Line'), ('Hit and Run', 'Hit and Run'), ('Threatening  Passenger', 'Threatening  Passenger'), ('Reckless Driving', 'Reckless Driving'), ('Discriminating against passenger', 'Discriminating against passenger'), ('Refusal to grant discount', 'Refusal to grant discount'), ('Others', 'Others')])),
                ('date', models.DateTimeField()),
                ('location', models.CharField(max_length=256)),
                ('created_at', models.DateTimeField(null=True, auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(null=True)),
                ('picture', models.CharField(max_length=128, null=True, blank=True)),
                ('description', models.CharField(max_length=2000, null=True)),
                ('user', models.ForeignKey(to='users.User')),
            ],
        ),
    ]
