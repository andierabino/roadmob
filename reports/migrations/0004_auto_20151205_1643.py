# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import reports.models


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0003_auto_20151119_1624'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='plate_number',
            field=models.CharField(validators=[reports.models.PlateNumberValidator()], max_length=8, db_index=True),
        ),
    ]
