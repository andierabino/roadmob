# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0002_auto_20151117_1532'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='plate_number',
            field=models.CharField(max_length=8, db_index=True),
        ),
    ]
