import os

from django.http import HttpResponseRedirect, Http404
from django.views.generic import TemplateView, DetailView
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.decorators import method_decorator
from django.conf import settings

from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response

from .models import Report
from .serializers import ReportSerializer


def my_login_required(function):
    def wrapper(request, *args, **kw):
        user=request.user
        request.session['next'] = request.path
        if not (user.id):
            return HttpResponseRedirect(reverse('user:auth'))
        else:
            return function(request, *args, **kw)
    return wrapper

def create(request):
    return HttpResponseRedirect('{}#report/new'.format(reverse('user:index')))


def about(request):
    return HttpResponseRedirect('{}#about'.format(reverse('user:index')))

def report_detail(request, pk):
    return HttpResponseRedirect('{}#report/{}'.format(reverse('user:index'), pk,))


def insights(request):
    return HttpResponseRedirect('{}#insights'.format(reverse('user:index')))


def search(request):
    params = request.GET.copy()
    query = ''
    if 'q' in params:
        if params['q']:
            query += 'q=' + params['q']
            return HttpResponseRedirect('{}#search?{}'.format(reverse('user:index'), query,))

    return HttpResponseRedirect('{}'.format(reverse('user:index'),))


class ReportDetailView(DetailView):

    template_name = 'page.html'
    queryset = Report.objects
    def get_context_data(self, **kwargs):
        context = super(ReportDetailView, self).get_context_data(**kwargs)

        report = self.object
        og_tags = {'fb:app_id': settings.FB['APP_ID'],
                   'og:type': 'article',
                   'article:publisher': 'https://www.facebook.com/roadmobph',
                   'article:tag': 'report',
                   'og:site_name': 'Roadmob',
                   'og:title': report.plate_number + ' has been Roadmobbed.',
                   'og:description': report.description,
                   'og:url': self.request.build_absolute_uri(reverse('report:detail', kwargs={'pk':report.id})),
                   }


        tw_cards = {'twitter:card': 'summary_large_image',
                    'twitter:site': '@roadmobph',
                    'twitter:title':  report.plate_number + ' has been Roadmobbed.'
                    }
        if report.picture:
            og_tags['og:image'] = 'http://'+report.picture
            tw_cards['twitter:image'] = 'http://'+report.picture
        else:
            picture = settings.HOST + 'static/app/images/roadmob-splash.png'
            og_tags['og:image'] = picture
            tw_cards['twitter:image'] = picture

        context['title'] = report.plate_number + ' has been Roadmobbed.'
        context['og_tags'] = og_tags
        context['tw_cards'] = tw_cards

        return context


def dashboard(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('{}#dashboard'.format(reverse('user:index')))
    else:
        return HttpResponseRedirect('{}'.format(reverse('user:index')))
