# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(verbose_name='password', max_length=128)),
                ('last_login', models.DateTimeField(verbose_name='last login', null=True, blank=True)),
                ('user_id', models.UUIDField(default=uuid.uuid4)),
                ('email', models.EmailField(max_length=255, null=True, unique=True)),
                ('contact', models.CharField(max_length=50)),
                ('name', models.CharField(max_length=128)),
                ('registered_at', models.DateTimeField(db_index=True, auto_now_add=True)),
                ('unregistered_at', models.DateTimeField(null=True, db_index=True)),
                ('fb_id', models.CharField(max_length=100, null=True, db_index=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
