# -*- coding: utf-8 -*-
from rest_framework import serializers

from .models import User

class UserSerializer(serializers.ModelSerializer):

   class Meta:
    model = User
    registered_at_text = serializers.CharField(read_only=True, source='registered_at_text')
    fields = ('name', 'id', 'fb_id', 'registered_at_text')
