# -*- coding: utf-8 -*-
import urllib
import json

from django.contrib.auth import authenticate, login as auth_login, logout

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework import status
from rest_framework.decorators import permission_classes, detail_route

from .models import User
from .serializers import UserSerializer


class LoginAPI(APIView):
    def get(self, request):
        if request.user.is_authenticated():
            user_data = {
                'id': request.user.user_id,
                'name': request.user.name(),
                'fb_id': request.user.fb_id,
                'registered_at': request.user.registered_at_text()
            }
            return Response({'user':user_data},status.HTTP_200_OK)

        else:
            return Response({},status.HTTP_200_OK)

    def post(self, request):
        fb_id = request.POST.get('fb_id')
        access_token = request.POST.get('access_token')
        fields = 'first_name,last_name,email'
        try:
            user = User.objects.get(fb_id=fb_id)
        except User.DoesNotExist:
            USER_URL = 'https://graph.facebook.com/{fb_id}?fields={fields}&access_token={access_token}'.format(
                fb_id = fb_id,
                fields = fields,
                access_token = access_token
            )
            response = urllib.request.urlopen(USER_URL)
            user_data = json.loads(response.read().decode())

            user = User()
            user.fb_id = fb_id
            user.firstname = user_data.get('first_name', '')
            user.lastname = user_data.get('last_name', '')
            user.email = user_data.get('email', '')
            user.save()

        try:
            user = authenticate(fb_id=fb_id)
            auth_login(request, user)
            if request.user.is_authenticated():
                user_data = {
                    'id': user.user_id,
                    'firstname': user.firstname,
                    'lastname': user.lastname,
                    'name': user.name(),
                    'fb_id': user.fb_id,
                    'registered_at': user.registered_at_text()
                }
                return Response({'user':user_data},status.HTTP_200_OK)
            else:
                return Response({},status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print(e.message)

class LogoutAPI(APIView):
    def get(self, request):
        if request.user.is_authenticated():
            logout(request)

            if not request.user.is_authenticated():
                return Response({},status.HTTP_200_OK)

        return Response({},status.HTTP_200_OK)

class UserContactDetailsAPI(APIView):
    def get(self, request):
        if request.user.is_authenticated():
            return Response({"email": request.user.email, "contact": request.user.contact})
        return Response({},status.HTTP_400_BAD_REQUEST)

class UserViewSet(viewsets.ViewSet):

    serializer_class = UserSerializer
    queryset = User.objects.all().order_by('-registered_at')

    #get data from database
    def retrieve(self, request, pk):
        import uuid
        if request.user.user_id == uuid.UUID(pk):
            serializer = UserSerializer(self.queryset.get(user_id=uuid.UUID(pk)),many=False)
            return Response({"data":serializer.data})
        return Response({},status.HTTP_400_BAD_REQUEST)

    #create instance of the model
    def create(self, request): #implement this
        pass

    #modify entry in database - all fields
    def update(self, request):
        pass

    #modify at least 1 field
    def partial_update(self, request):
        pass

    #remove from database
    def destroy(self, request):
        pass

    #search report
    @detail_route()
    def email(self, request):
        if request.user.is_authenticated():
            return Response({"email": request.user.email})
        return Response({},status.HTTP_400_BAD_REQUEST)
