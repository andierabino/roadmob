# -*- coding: utf-8 -*-
import uuid

from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

import roadmob.utils as utils

class UserManager(BaseUserManager):
    def create_user(self, user_id, email, contact, firstname, lastname, fb_id, password=None, **extra_fields):
        user = User()
        user.fb_id = fb_id
        user.firstname = firstname
        user.lastname = lastname
        user.email = email
        user.contact = contact
        user.save()
        return user

    def create_superuser(self, email, password, user_id=-1, contact=-1, firstname="admin", lastname="admin", fb_id=-1, **extra_fields):
        user = User()
        user.fb_id = fb_id
        user.firstname = firstname
        user.lastname = lastname
        user.email = email
        user.contact = contact
        user.set_password(password)
        user.is_active = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser):
    user_id = models.UUIDField(max_length=40, default=uuid.uuid4)
    email = models.EmailField(max_length=255, unique=True, null=True)
    contact = models.CharField(max_length=50)
    firstname = models.CharField(max_length=128, null=True)
    lastname = models.CharField(max_length=128, null=True)
    registered_at = models.DateTimeField(auto_now_add=True, db_index=True)
    unregistered_at = models.DateTimeField(null=True, db_index=True)
    fb_id = models.CharField(max_length=100, null=True, db_index=True)
    USERNAME_FIELD = 'email'

    objects = UserManager()
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    def registered_at_text(self):
        return utils.datetext(self.registered_at)

    def name(self):
        return self.firstname + ' ' + self.lastname

    # extra dummy functions needed for createsuperuser
    def has_perm(self, perm, obj=None):
        return self.is_superuser

    def has_module_perms(self, app_label):
        return self.is_superuser

    def get_short_name(self):
        return self.firstname