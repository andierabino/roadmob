# -*- coding: utf-8 -*-
# Stdlib imports
from __future__ import absolute_import
import json
import urllib
import hashlib
import os

# Core Django imports
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login as auth_login
from django.conf import settings

# Third-party app imports
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view

# App imports
from users.models import User
from users.api import UserViewSet

# url for sharing using localhost, delete later
# run ngrok http 8000, then get link
#TUNNEL_URI = "http://localhost:8000"
TUNNEL_URI = "http://3798f211.ngrok.io/"

FB_CONFIG = {
    'APP_ID' : settings.FB['APP_ID'],
    'APP_SECRET' : settings.FB['APP_SECRET'],
    'REDIRECT_URI' : settings.HOST + 'login',
    'REPORT_URI' : settings.HOST + 'report/',
    'SCOPE' : 'public_profile, email, user_friends'
}

# Create your views here.
def index(request):
    context_dict = {}
    context_dict['FB_APP_ID'] = FB_CONFIG['APP_ID']
    context_dict['REPORT_URI'] = FB_CONFIG['REPORT_URI']

    if 'development' in os.environ.get('DJANGO_SETTINGS_MODULE', ''):
        return render(request, 'index.html', context_dict)
    else:
        return render(request, 'dist/index.html', context_dict)

def raw_index(request):
    context_dict = {}
    context_dict['FB_APP_ID'] = FB_CONFIG['APP_ID']
    context_dict['REPORT_URI'] = FB_CONFIG['REPORT_URI']
    return render(request, 'users/index.html', context_dict)


def auth(request):

    state = hashlib.sha256(os.urandom(1024)).hexdigest()
    request.session['state'] = state

    auth_uri = 'https://www.facebook.com/dialog/oauth?state={state}&client_id={app_id}&scope={scope}&redirect_uri={redirect_uri}'.format(
        state = state,
        app_id = FB_CONFIG['APP_ID'],
        redirect_uri = FB_CONFIG['REDIRECT_URI'],
        scope = FB_CONFIG['SCOPE']
        )


    return HttpResponseRedirect(auth_uri)


def fblogin(request):
    if request.GET.get('error') or request.session['state'] != request.GET.get('state'):
        return redirect('/')
    else:
        code = request.GET.get('code')

    #Exchange code for token
    GET_TOKEN_URI = 'https://graph.facebook.com/v2.3/oauth/access_token'
    GET_TOKEN_URL = '{get_token_uri}?client_id={app_id}&client_secret={app_secret}&redirect_uri={redirect_uri}&code={code}'.format(
        get_token_uri = GET_TOKEN_URI,
        app_id = FB_CONFIG['APP_ID'],
        app_secret = FB_CONFIG['APP_SECRET'],
        redirect_uri = FB_CONFIG['REDIRECT_URI'],
        code = code
        )

    try:
        response = urllib.request.urlopen(GET_TOKEN_URL)
    except:
        return redirect('/')

    response_data = json.loads(response.read().decode())
    access_token = response_data['access_token']

    DEBUG_TOKEN_URL = 'https://graph.facebook.com/debug_token?input_token={user_token}&access_token={app_token}&redirect_uri={redirect_uri}'.format(
        user_token = access_token,
        app_token = FB_CONFIG['APP_ID'] + '|' + FB_CONFIG['APP_SECRET'],
        redirect_uri = FB_CONFIG['REDIRECT_URI'],
        )

    response = urllib.request.urlopen(DEBUG_TOKEN_URL)
    token_info = json.loads(response.read().decode())
    token_info = token_info['data']


    fb_id = token_info['user_id']

    fields = 'first_name,last_name'
    if 'email' in token_info['scopes']:
        fields = fields + ',email'

    USER_URL = 'https://graph.facebook.com/{fb_id}?fields={fields}&access_token={access_token}&redirect_uri={redirect_uri}'.format(
        fb_id = fb_id,
        fields = fields,
        access_token = access_token,
        redirect_uri = FB_CONFIG['REDIRECT_URI']
    )



    response = urllib.request.urlopen(USER_URL)
    user_data =    json.loads(response.read().decode())

    #checks if user already has an account, if no = create user model

    try:
        user = User.objects.get(fb_id=fb_id)
    except User.DoesNotExist:
        user = User()
        user.fb_id = fb_id
        user.name = user_data.get('first_name', '') + ' ' + user_data.get('last_name', '')
        user.email = user_data.get('email', '')
        user.save()

    user = authenticate(fb_id=fb_id)
    auth_login(request, user)

    #return redirect(reverse('user:index'))


    next = '/'
    if 'next' in request.session:
        next = request.session['next']
        del request.session['next']
    return redirect(next)

def userprofile(request):
    context_dict = {}
    context_dict['FB_APP_ID'] = FB_CONFIG['APP_ID']
    context_dict['REPORT_URI'] = FB_CONFIG['REPORT_URI']
    return render(request, 'users/', context_dict)
    return HttpResponseRedirect('{}#user/profile/{}'.format(reverse('user:index'), pk,))
