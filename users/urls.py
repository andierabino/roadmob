from django.conf.urls import url

from rest_framework import routers

from users import views, api
from .api import UserViewSet

router = routers.SimpleRouter()
router.register(r'^api/user', UserViewSet)

slashlessRouter = routers.SimpleRouter(trailing_slash=False)
slashlessRouter.register(r'^api/user', UserViewSet)

urlpatterns = [
    url('^$',views.index, name='index'),
    url(r'^unbuilt$',views.raw_index, name='raw-index'),
    url(r'^auth', views.auth, name='auth'),
    url(r'^login',views.fblogin, name='fblogin'),
    url(r'^logout/$',
        'django.contrib.auth.views.logout',
        name='logout',
        kwargs={'next_page': '/'}),
    url(r'^api/login$', api.LoginAPI.as_view(), name="fb-login"),
    url(r'^api/logout$', api.LogoutAPI.as_view(), name="logout"),
    url(r'^api/user/contact_details$', api.UserContactDetailsAPI.as_view(), name="user-contact"),

]

urlpatterns += router.urls
urlpatterns += slashlessRouter.urls
