define([
  'commons/util'
], function(util) {

  return {
   /**
    * Initializes api
    */
    init : function () {
      this.prefilter();
    },

   /**
    * Prefilters ajax request for csrf protection
    */
    prefilter : function() {
      var csrftoken = util.getCookie('csrftoken');
      $.ajaxPrefilter(function (options) {
        if (!options.beforeSend) {
          options.beforeSend = function (xhr, settings) {
            if (!util.csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
          }
        }

        if (!options.complete) {
          options.complete = function (xhr, textStatus) {
            if (xhr.status == 403) {
              window.location.reload(true)
            }
          }
        }
      });


    }
  };

});
