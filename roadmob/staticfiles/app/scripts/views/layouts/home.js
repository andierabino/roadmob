define([
  'marionette',
  'backbone',
  'tpl!templates/layouts/home.tpl',
  'views/composites/home.reports',
  'vent',
  'views/items/home.loginModal',
  'views/items/home.hero'
], function (Marionette, Backbone, tpl, ReportCollectionView, vent, LoginModalView, HeroView) {
  'use strict';

  return Marionette.LayoutView.extend({
    template: tpl,
    initialize: function () {
      this.listenTo(vent, 'show:loginModal', this.showLoginModal);
      $('#header-region').addClass('on-landing');
    },
    regions: {
      reportCollection: '#report-collection-region',
      about: '#about-panel',
      contact: '#contact-panel',
      overlay: '#modal-overlay',
      hero: '#hero-region'
    },
    ui: {
      searchInput: '#search-input',
      searchButton: '#search-button',
      fileReportButton: '#file-main'
    },
    events: {
      'click @ui.searchButton': 'search',
      'click @ui.fileReportButton': 'fileReport',
      'keyup @ui.searchInput': 'searchOnKeyup'
    },
    fileReport: function (e) {
      e.preventDefault();
      vent.trigger('checklogin:filereport', e);
      return false;
    },
    searchOnKeyup: function (e) {
      var self = this;
      if (e.which === 13) {
        self.search();
      }
    },
    search: function () {
      var self = this;
      var query = self.ui.searchInput.val();
      if (query) {
        Backbone.history.navigate('search?q='+query, {trigger: true, replace: false});
      }
    },
    onShow: function () {
      var self = this;
      self.reportCollection.show(new ReportCollectionView());
      self.hero.show(new HeroView());

      $('body').delegate('#modal-overlay,.modal-close', 'click', function(event) {
        event.stopPropagation();
        self.hideModal();
      });


    },
    showLoginModal: function () {
      var self = this;
      $('#modal-overlay').addClass('modal-overlay-visible');
      self.overlay.show(new LoginModalView());
    },
    hideModal: function () {
      $('.modal-fixed-center').fadeOut(500);
      $('#modal-overlay').removeClass('modal-overlay-visible');
    }
  })
})
