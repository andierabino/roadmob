define([
  'marionette',
  'tpl!templates/layouts/reportCreate.tpl',
  'views/composites/report.form'
], function (Marionette, tpl, ReportCreateView) {
  'use strict';

  return Marionette.LayoutView.extend({
    template: tpl,
    regions: {
      reportCreate: '#report-create-region'
    },
    onShow: function () {
      var self = this;
      self.reportCreate.show(new ReportCreateView());
    }
  })
})
