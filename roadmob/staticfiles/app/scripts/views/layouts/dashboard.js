define([
  'marionette',
  'backbone',
  'tpl!templates/layouts/dashboard.tpl',
  'models/user',
  'views/items/dashboard.userProfile',
  'views/composites/dashboard.recentReports',
  'views/composites/dashboard.myReports',
  'models/report',
  'views/items/reportDetail.report'
], function (
    Marionette,
    Backbone,
    tpl,
    UserModel,
    UserProfileView,
    RecentReportsView,
    UserReportsView,
    ReportModel,
    ReportDetailView) {
  'use strict';

  return Marionette.LayoutView.extend({
    template: tpl,
    regions: {
      userProfile: '#user-profile-region',
      recentReports: '#recent-reports-region',
      userReports: '#user-reports-region',
      reportModal: '#report-modal-region'
    },
    childEvents: {
      'report:clicked': 'viewReport',
      'report:closed': 'closeReport'
    },
    onShow: function () {
      var self = this;
      self.userProfile.show(new UserProfileView());
      self.recentReports.show(new RecentReportsView());
      self.userReports.show(new UserReportsView());
    },
    initialize: function () {
      var self = this;
    },
    viewReport: function (child) {
      var self = this;
      var report = new ReportModel({id: child.model.get('id')});
      var reportDetail = new ReportDetailView({model: report})
      reportDetail.model.fetch();
      self.reportModal.show(reportDetail);
      self.reportModal.$el.removeClass('hidden');
      $('body').css('position', 'fixed');
      $('body').css('overflow', 'hidden');
    },
    closeReport: function (child) {
      var self = this;
      self.reportModal.$el.addClass('hidden');
      self.reportModal.reset();
      $('body').css('position', 'relative');
      $('body').css('overflow-x', 'hidden');
      $('body').css('overflow-y', 'auto');
    }
  })
})
