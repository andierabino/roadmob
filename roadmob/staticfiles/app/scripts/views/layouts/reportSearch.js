define([
  'marionette',
  'tpl!templates/layouts/reportSearch.tpl',
  'views/composites/search.reports'
], function (Marionette, tpl, ReportSearchView) {
  'use strict';

  return Marionette.LayoutView.extend({
    template: tpl,
    regions: {
      reportSearch: '#report-search-region'
    },
    params: null,
    initialize: function (params) {
      var self = this;
      self.params = params;
      delete self.params['destroyImmediate'];
    },
    onShow: function () {
      var self = this;
      self.reportSearch.show(new ReportSearchView(self.params));
    }
  });
})
