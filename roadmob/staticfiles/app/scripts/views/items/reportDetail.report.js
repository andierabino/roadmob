define([
  'marionette',
  'views/items/report',
  'tpl!templates/items/reportDetail.report.tpl'
], function (Marionette, ReportView, tpl) {

  return ReportView.extend({
    template: tpl,
    ui: {
      shareFB: '.share-facebook',
      shareTwitter: '.share-twitter',
      reportClose: '.modal-close'
    },
    events: {
      'click @ui.shareFB': 'shareFB',
      'click @ui.shareTwitter': 'shareTwitter'
    },
    modelEvents : {
      'change' : 'render',
      'sync' : 'render'
    },
    triggers: {
        "click @ui.reportClose": "report:closed"
    },
    onRender: function () {
      if(G.loadedFB)
        FB.XFBML.parse();
    }
  });
});
