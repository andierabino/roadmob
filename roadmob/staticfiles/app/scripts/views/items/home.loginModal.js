define([
  'marionette',
  'backbone',
  'tpl!templates/items/home.loginModal.tpl',
  'vent'
], function (Marionette, Backbone, tpl, vent) {

  return Marionette.ItemView.extend({
    template: tpl,
    className: 'modal-fixed-center',
    ui: {
      fbLoginButton: '.modal-fb-login'
    },
    events: {
      'click @ui.fbLoginButton': 'fbLogin'
    },
    fbLogin: function (e) {
      vent.trigger('login', e);
    }
  });
});
