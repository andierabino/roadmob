define([
  'marionette',
  'backbone',
  'tpl!templates/items/menu.tpl',
  'models/user',
  'vent'
], function (Marionette, Backbone, tpl, UserModel, vent) {

  var instance = null;
  var menu = Marionette.ItemView.extend({
    template: tpl,
    ui: {
      logoutButton: '#logout'
    },
    events: {
      'click @ui.logoutButton': 'logout'
    },
    initialize: function () {
      var self = this;
      self.model = UserModel.getLoggedUser();
      self.listenTo(self.model, "change", self.render);
      self.model.checkLoginState();
      self.listenTo(vent, 'logout', function () {
        self.logout();
      });
      self.listenTo(vent, 'login', function(e) {
        self.model.fbLogin(function () {
          self.model.fetch();

          if (e&&e.target!=undefined) {
            if ($(e.target).data('next') == 'file-report') {
              Backbone.history.navigate('report/new', {trigger: true, replace: false});
            } else {
              Backbone.history.navigate('dashboard', {trigger: true, replace: false});
            }
          } else {
            Backbone.history.navigate('dashboard', {trigger: true, replace: false});
          }


        });
      });

      self.listenTo(vent, 'checklogin:filereport', function() {
        self.model.checkLoginState(function (user) {

          if (user) {
            Backbone.history.navigate('report/new', {trigger: true, replace: false});
          } else {
            vent.trigger('show:loginModal');
          }

        });

      });
    },
    logout: function () {
      var self = this;
      self.model.logout();
      self.hideMenu();
    },
    hideMenu: function () {
      $('#site-wrapper').removeClass('show-nav');
    },

  }, {
    getInstance: function () {
      if (instance == null) {
        instance = new menu();
      }

      return instance;
    }
  });

  return menu;
});
