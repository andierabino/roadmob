define([
  'marionette',
  'backbone',
  'tpl!templates/items/dashboard.userProfile.tpl',
  'models/user',
  'vent'
], function (Marionette, Backbone, tpl, UserModel, vent) {

  return Marionette.ItemView.extend({
    template: tpl,
    initialize: function () {
      var self = this;
      self.model = UserModel.getLoggedUser();
      self.listenTo(self.model,"change",self.render);
    }
  });
});
