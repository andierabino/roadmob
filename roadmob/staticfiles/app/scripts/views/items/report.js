define([
  'marionette',
  'backbone'
], function (Marionette, Backbone) {

  return Marionette.ItemView.extend({
    shareFB: function (event) {
      event.stopPropagation();
      var self = this;
      var link = REPORT_URI + self.model.get('id');
      if (G.loadedFB) {
        FB.ui({
          method: 'share',
          href: link
        });
      }
    },
    shareTwitter: function (event) {

      var self = this;
      var link = "https://twitter.com/intent/tweet?text=" + self.model.get('plate_number') + "+just+got+RoadMobbed!"
                  + "&url=" + REPORT_URI + "/" + self.model.get('id');
      var x = screen.width/2 - 700/2;
      var y = screen.height/2 - 485/2;
      window.open(encodeURI(link), "Roadmob Twitter Share", 'height=485,width=700,left='+x+',top='+y);
      event.stopPropagation();
    },
    viewReport: function () {
      var self = this;
      router.navigate('report/'+self.model.get('id'), {trigger: true, replace: false});
    }
  });
});
