define([
  'marionette',
  'views/items/report',
  'tpl!templates/items/home.report.tpl'
], function (Marionette, ReportView, tpl) {

  return ReportView.extend({
    template: tpl,
    className: 'dashboard-report',
    ui: {
      shareFB: '.share-facebook',
      shareTwitter: '.share-twitter',
      reportTitle: '.report-card-title',
      reportCard: '.dashboard-report-inner'
    },
    events: {
      'click @ui.shareFB': 'shareFB',
      'click @ui.shareTwitter': 'shareTwitter',
      'click @ui.reportTitle': 'viewReport',
      'click @ui.reportCard': 'viewReport'
    }
  });
});
