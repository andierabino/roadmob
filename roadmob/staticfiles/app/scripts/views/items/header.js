define([
  'marionette',
  'backbone',
  'tpl!templates/items/header.tpl',
  'models/user',
  'vent',
  'views/items/menu'
], function (Marionette, Backbone, tpl, UserModel, vent, MenuView) {
  'use strict';

  var instance = null;
  var header = Marionette.ItemView.extend({
    template: tpl,
    initialize: function () {
      var self = this;
      self.model = UserModel.getLoggedUser();
      self.listenTo(self.model,"change",self.render);
    },

    className: 'grid-container',
    ui: {
      searchButton: '#roadmob-search-button',
      searchInput: '#roadmob-search-input',
      fbLoginButton: '#fb-login',
      userToggle: '.menu-button',
      userOptions: '.menu-dropdown',
      logoutButton: '.logout'
    },
    events: {
      'click @ui.searchButton': 'search',
      'keyup @ui.searchInput': 'searchOnKeyup',
      'click @ui.fbLoginButton': 'fbLogin',
      'click @ui.logoutButton': 'logout',
      'click @ui.userToggle': 'showMenu'
    },
    logout: function () {
      vent.trigger('logout');
    },
    fbLogin: function (e) {
      vent.trigger('login', e);
    },
    modelEvents : {
      'change' : 'render'
    },
    search: function () {
      var self = this;
      var query = self.ui.searchInput.val();
      if (query)
        router.navigate('search?q='+query, {trigger: true, replace: false});
    },
    showMenu: function () {
      var self = this;
      if (window.innerWidth <= 767) {
        $('#site-wrapper').toggleClass('show-nav');
      } else {
        $('#user-menu .menu-dropdown').toggleClass('visible');
      }


    },
    hideDropDown: function () {
      if (window.innerWidth > 767) {
        $('#user-menu .menu-dropdown').toggleClass('visible');
      }
    },
    hideMenu: function () {
      $('#site-wrapper').removeClass('show-nav');
    },
    searchOnKeyup: function (e) {
      var self = this;
      if (e.which === 13) {
        self.search();
      }
      if (e.which === 8) {
        if(!self.ui.searchInput.val()) {
          //Backbone.history.navigate('/', {trigger: true, replace: false});
        }
      }
    }
  }, {
    getInstance: function () {
      if (instance == null) {
        instance = new header();
      }

      return instance;
    }
  });

  return header;
});
