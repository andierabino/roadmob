define([
  'marionette',
  'views/items/report',
  'tpl!templates/items/dashboard.report.tpl'
], function (Marionette, ReportView, tpl) {

  return ReportView.extend({
    template: tpl,
    className: 'dashboard-report grid-parent grid-100',
    ui: {
      shareFB: '.share-facebook',
      shareTwitter: '.share-twitter',
      reportTitle: '.report-title',
      reportCard: '.dashboard-report-inner'
    },
    events: {
      'click @ui.shareFB': 'shareFB',
      'click @ui.shareTwitter': 'shareTwitter'
    },
    triggers: {
        "click @ui.reportCard": "report:clicked"
    }
  });
});
