define([
  'marionette',
  'tpl!templates/composites/search.reports.tpl',
  'views/items/search.report',
  'models/reports'
], function (Marionette, tpl, ReportItemView, ReportCollection) {
  return Marionette.CompositeView.extend({
    template: tpl,
    childView: ReportItemView,
    childViewContainer: '#reports',
    ui: {
      searchButton: '#roadmob-search-button',
      searchInput: '#roadmob-search-input',
      searchResultsText: '#search-results-text'
    },
    events: {
      'click #roadmob-search-input': 'search',
      'keyup #roadmob-search-button': 'searchOnKeyup'
    },
    searchOnKeyup: function (e) {
      var self = this;
      if (e.which === 13) {
        self.search();
      }
    },
    search: function () {
      var self = this;
      var query = self.ui.searchInput.val();
      if (query) {
        self.collection.fetch({
          data: {q: query},
          processData: true,
          beforeSend: function () {
            $('body').append('<div class="overlay white-overlay"><div class="overlay-loading"></div></div>');
          },
          complete: function () {
            $('.overlay').remove();
          }
        });
      }
    },
    initialize: function (params) {
      var self = this;
      self.params = params;
      self.collection = new ReportCollection();
      if ('q' in params) {
        self.collection.fetch({
          data: {q: params['q']},
          processData: true,
          beforeSend: function () {
            $('body').append('<div class="overlay white-overlay"><div class="overlay-loading"></div></div>');
          },
          success: function (collection) {
            self.updateResultsText(collection.length);
          },
          complete: function () {
            $('.overlay').remove();
          }
        });
      }

    },
    updateResultsText: function (count) {
      var self = this;
      var text = count <= 1 ? 'result' : 'results';
      self.ui.searchResultsText.text('Found ' + count + ' ' + text + '.');
    },
    onRender: function () {
      var self = this;
      if ('q' in self.params) {
        var searchInput = document.getElementById('roadmob-search-input');
        searchInput.value = self.params['q'];
        searchInput.focus();
      }
    }
  });
})
