define([
  'marionette',
  'views/composites/dashboard.recentReports',
  'tpl!templates/composites/dashboard.myReports.tpl',
  'models/reports'
], function (Marionette, ReportsView, tpl, ReportCollection) {

  return ReportsView.extend({
    template: tpl,
    initialize: function () {
      var self = this;

      self.collection = new ReportCollection();

      self.collection.fetch({
        data: {
          limit: 1,
          user: G.me.id
        },
        success: function (collection) {
          if(collection.length == 0){
            self.ui.noReports.show();
          }
        },
        complete: function () {
          self.showHasMore();
        }
      });
    }
  });
});
