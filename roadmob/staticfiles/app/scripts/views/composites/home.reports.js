define([
  'marionette',
  'tpl!templates/composites/home.reports.tpl',
  'views/items/home.report',
  'models/reports'
], function (Marionette, tpl, ReportItemView, ReportCollection) {
  return Marionette.CompositeView.extend(
    {
      template: tpl,
      className: 'grid-100',
      childView: ReportItemView,
      childViewContainer: '#reports-collection',
      ui: {
        reportsLoader: '.reports-loading',
        seeMore: '.see-more'
      },
      events: {
        'click @ui.seeMore': 'loadMore'
      },
      initialize: function () {
        var self = this;
        self.collection = new ReportCollection();
        self.collection.fetch({
          data: {
            limit: 10
          },
          complete: function () {
            self.ui.reportsLoader.hide();
          }
        });
      },
      onRender: function () {
        var self = this;
        self.showHasMore();
        
      },
      showHasMore: function () {
        var self = this;
        if (!self.collection.has_more) {
          self.ui.seeMore.hide();
        } else {
          self.ui.seeMore.show();
        }
      },
      loadMore: function () {
        var self = this;
        var lastModel = self.collection.at(self.collection.length - 1);
        var lastDate = lastModel.get('created_at','');
        self.collection.fetch({
          remove: false,
          data: {
            limit: 10,
            last: lastDate
          },
          success: function (collection) {

          },
          complete: function () {
            self.showHasMore();
          }
        });
      }
    }

  )
});
