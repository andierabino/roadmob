define([
  'marionette',
  'backbone',
  'tpl!templates/composites/report.form.tpl'
], function (Marionette, Backbone, tpl, google) {
  return Marionette.CompositeView.extend({
    template: tpl,

    ui: {
      submitBtn: '#submit-report',
      inputPlateNumber: 'input[name="plate_number"]',
      inputOperator: 'input[name="operator"]',
      inputVehicleType: 'select[name="vehicle_type"]',
      inputComplaintType: 'select[name="complaint_type"]',
      inputDate: 'input[name="date"]',
      inputLocation: 'input[name="location"]',
      inputDescription: 'textarea[name="description"]',
      inputContact: 'input[name="contact"]',
      inputEmail: 'input[name="email"]'
    },
    events: {
      'click @ui.submitBtn': 'submit'
    },
    form: {
      inputPlateNumber: ['required', 'platenumber'],
      inputOperator: ['alphanumspecial'],
      inputVehicleType: ['required'],
      inputComplaintType: ['required'],
      inputDate: ['required'],
      inputLocation: ['required', 'alphanumspecial'],
      inputDescription: ['alphanumspecial'],
      inputContact: ['required', 'num'],
      inputEmail: ['required', 'email']
    },
    optional: [
      'inputDescription',
      'inputOperator'
    ],
    rules: {
      required: function (str) {
        return str!='' && str!=null;
      },
      alphanumspecial: function (str) {
        var pattern = /^[ A-Za-z0-9_@'.,\#&+-ñáéíóúü]+$/i;
        return pattern.test(str);
      },
      num: function (str) {
        var pattern = /^\d+$/;
        return pattern.test(str);
      },
      platenumber: function (str) {
        var pattern = /^[A-za-z]{2,3}\-*[0-9]{3,5}$/;
        return pattern.test(str);
      },
      email: function (str) {
        var pattern = new RegExp('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$', 'i');
        return pattern.test(str);
      }
    },
    errorMessages : {
      required: "This field may not be blank.",
      alphanumspecial: "This field must only contain spaces, alphanumeric characters or _@'./#&+-",
      platenumber: "A valid plate number is required.",
      num: "This field must only contain numbers.",
      email: "Email address must be valid."
    },
    onRender: function () {
      var self = this;
      require(['googlemaps!'], function(googleMaps) {
        var input = /** @type {!HTMLInputElement} */(
              document.getElementById('location'));
        var autocomplete = new googleMaps.places.Autocomplete(input);
      });

      $.ajax({
        method: 'GET',
        url: '/api/user/contact_details',
        success: function (data) {
          self.ui.inputContact.val(data['contact']);
          self.ui.inputEmail.val(data['email']);
        }
      });
    },

    submit: function () {

      var self = this;
      var invalid = 0;
      $.each(self.form, function (key, val) {
        var validate = true;
        var str = self.ui[key].val();
        if (str==="") {
          if (self.optional.indexOf(key) > -1) {

            validate = false;
          }
        }

        if (validate) {
          $.each(val, function (i, rule) {
            var valid = self.rules[rule](str);
            if (!valid) {
              invalid++;
              self.ui[key].addClass('input-danger');
              self.ui[key].after('<div class="grid-100 grid-parent error-text">'+self.errorMessages[rule] +'</div>');
              return false;
            }
          })
        }

      });
      if (invalid)
        return false;

      else {

        var data = new FormData();
        data.append('report', JSON.stringify({
            plate_number: self.ui.inputPlateNumber.val(),
            vehicle_type: self.ui.inputVehicleType.val(),
            operator: self.ui.inputOperator.val(),
            incident: self.ui.inputComplaintType.val(),
            date: self.ui.inputDate.val(),
            location: self.ui.inputLocation.val(),
            description: self.ui.inputDescription.val()
          }))

        data.append('userinfo', JSON.stringify({
            contact: self.ui.inputContact.val(),
            email: self.ui.inputEmail.val()
          }))
        data.append('picture', $('input[name="picture"]')[0].files[0]);
        $.ajax({
          url: '/api/report',
          method: 'POST',
          contentType: false,
          processData: false,
          data: data,
          success: function () {
            Backbone.history.navigate('', {trigger: true});
          },
          error: function (xhr, ajaxOptions, thrownError) {
            var data = $.parseJSON(xhr.responseText);
            for (var key in data) {
              var errors = data[key];
              $('.error-'+key).html();
              for (var i in errors) {
                $('input#'+key).addClass('input-danger');
                $('.error-'+key).html(errors[i] + '\n');
              }
            }
          }
        })
      }

    }
  });
})
