define([
  'backbone',
], function (Backbone) {
  'use strict';

  return Backbone.Collection.extend({
    url: '/api/report',
    parse: function (data) {
      this.has_more = data.has_more;
      return data.data;
    }
  })
})
