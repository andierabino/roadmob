define([
  'backbone',
], function (Backbone) {
  'use strict';

  return Backbone.Model.extend({
    urlRoot: '/api/report',
    defaults: {
      id: null,
      plate_number: null,
      operator: null,
      description: null,
      date: null,
      date_text: null,
      incident: null,
      picture: null
    },
    parse: function (data) {
      return data.data;
    }
  });
});
