define([
  'app',
  'commons/util'
], function (app, util) {
  'use strict';
  var data;

  function load (path, params) {
    require(['views/layouts/' + path], function (PageView) {
      if (data) {
        params = _.extend({}, params, {
          data: data
        });
      }
      app.main.show(new PageView(params, data));
      data = null;
    });
  }

  return {
    home: function () {
      load('home');
    },
    reportCreate: function () {
      load('reportCreate');
    },
    reportSearch: function (queryString) {
      var params = util.parseQueryString(queryString);
      load('reportSearch', params);
    },
    reportDetail: function (id) {
      load('reportDetail', {report_id: id});
    },
    dashboard: function () {
      load('dashboard');
    },
    setData : function(_data) {
      data = _data;
    }
  }
})
