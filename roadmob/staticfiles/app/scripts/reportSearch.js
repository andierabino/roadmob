(function($) {
    $.QueryString = (function(a) {
        if (a == "") return {};
        var b = {};
        for (var i = 0; i < a.length; ++i)
        {
            var p=a[i].split('=');
            if (p.length != 2) continue;
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
        }
        return b;
    })(window.location.search.substr(1).split('&'))
})(jQuery);

var ReportSearchModel = function () {
	var self = this;

	self.reports = ko.observableArray([]);
	self.searchQuery = ko.observable('');
	self.isLoading = ko.observable(false);
	self.resultCount = ko.computed(function() {
		return self.reports().length;	
	});
	self.init = function () {
		query = $.QueryString['q'];
		self.searchQuery(query)

		if (query) {
			$.ajax({
				method: "GET",
				url: '/api/report/search?q='+query,
				beforeSend: function () {
					self.isLoading(true);
				},
				success: function(result) {
					if (result.data.length > 0) {
						self.reports(ko.toJS(result.data));
					}
				},
				complete: function () {
					self.isLoading(false);
				}	
			})
		}

	}

	self.search = function () {
		var searchurl = [location.protocol, '//', location.host, location.pathname].join('');
		searchQuery = self.searchQuery();
		if (searchQuery) {
			var newurl = searchurl + "?q=" + searchQuery;
			window.history.pushState({path:newurl},'',newurl)
			$.ajax({
				method: "GET",
				url: '/api/report/search?q='+self.searchQuery(),
				beforeSend: function () {
					self.reports([]);
					self.isLoading(true);
				},
				success: function (result) {
					self.reports(ko.toJS(result.data));
				},
				complete: function () {
					self.isLoading(false);
				}
			})
		}
	}

	self.searchOnKeyUp = function (item, event) {
		if (event.keyCode == 13)
			self.search();
	}

	self.init();

}

//apply ko bindings to #reports-section
var reportSearchModel = new ReportSearchModel();
ko.applyBindings(reportSearchModel, document.getElementById("report-search"));