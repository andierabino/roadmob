define([
  'backbone',
  'marionette',
  'views/layouts/home',
  'views/items/header',
  'views/items/menu',
  'vent'
], function (Backbone, Marionette, HomeView, HeaderView, MenuView, vent) {
  'use strict';

  var app = new Marionette.Application();
  app.addRegions({
    header: "#header-region",
    menu: "#site-menu",
    main: "#main-region"
  });

  app.on('start', function () {
      Backbone.history.start({pushState : true});
      app.header.show(HeaderView.getInstance());
      app.menu.show(MenuView.getInstance());
  });

  var $body = $('body');

  $body.delegate('.link', 'click', function(event) {
    var target = $(event.currentTarget),
      replace = false;
    if (Backbone.history.getFragment() == 'menu') {
      replace = true;
    }

    if (!Backbone.history.getFragment() && target.attr('href')=='/') {
      Backbone.history.loadUrl();
    } else {
      router.navigate(target.attr('href'), {trigger : true, replace : replace});
    }

    $('#site-wrapper').removeClass('show-nav');
    event.preventDefault();
    event.stopPropagation();
  });

  $body.delegate('.site-menu-close', 'click', function(event) {
    $('#site-wrapper').removeClass('show-nav');
  });

  return app;
})
