<div class="grid-container v-padded-25">
  <div class="grid-90 prefix-5 v-padded-25 grid-parent">
    <div id="user-profile-region" class="grid-30 padded-bottom-25">
    </div>
    <div class="grid-70">
      <div id="user-reports-region">
      </div>
      <div id="recent-reports-region">
      </div>
    </div>
  </div>
</div>

<div id="report-modal-region" class="modal-region hidden">
</div>
