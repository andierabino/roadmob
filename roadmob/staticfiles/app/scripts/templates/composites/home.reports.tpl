<div class="grid-70 prefix-15 dashboard-region-heading v-padded-25 text-large text-center">Recent Reports</div>
<div class="grid-70 prefix-15 grid-parent dashboard-region">
  <div class="text-center">
    <div class="reports-loading div-loading div-loading-rolling-orange"></div>
  </div>

  <div id="reports-collection" class="grid-100 grid-parent">

  </div>

  <div class="text-center grid-100">
    <a class="see-more btn btn-normal btn-medium" style="display:none">See More</a>
  </div>
  </div>
</div>
