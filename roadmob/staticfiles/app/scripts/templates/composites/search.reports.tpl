<div class="grid-container v-padded-25" id="report-search">
	<div class="grid-70 prefix-15 dashboard-region no-heading grid-parent">
		<div class="grid-100 grid-parent v-padded-25">
	    <div class="text-large text-center"><b>Search Reports</b></div>
			<div class="grid-100 text-center grid-parent text-small">
				<b id="search-results-text"></b>
			</div>
	  </div>


		<div class="divider"></div>
		<div id="reports" class="grid-100 grid-parent">
		</div>
	</div>
</div>
