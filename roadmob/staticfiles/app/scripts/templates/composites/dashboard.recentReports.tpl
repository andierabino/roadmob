<div class="grid-container">
  <div class="dashboard-region-heading text-large text-center v-padded-15">Recent Reports</div>
  <div class="grid-100 grid-parent dashboard-region">

    <div class="grid-100 grid-parent">

      <div class="no-reports text-medium text-center v-padded-25" style="display:none">No reports to display.</div>
      <div id="reports-collection" class="grid-100 grid-parent">

      </div>
    </div>

    <div class="see-more text-center grid-100 v-padded-25" style="display:none">
      <a class="btn btn-default btn-small btn-medium">See More</a>
    </div>
  </div>
</div>
