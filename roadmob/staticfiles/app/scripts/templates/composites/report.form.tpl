<div class="grid-container max-10000">
  <div class="text-center heading-medium form-header grid-100 v-padded-50 grid-parent">File a Report</div>
</div>
<div class="grid-container">

  <div id="report-detail-section" class="grid-50 prefix-25 tablet-grid-90 tablet-prefix-5 mobile-grid-90 mobile-prefix-5 v-padded-50 padded-bottom">

      <div class="form grid-100 grid-parent">

        <div class="form-body">
          <div class="heading-medium">1. Report Details</div>

          <div class="input-group grid-100 tablet-grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
              Vehicle Type:
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">
              <select class="grid-100 tablet-grid-100 mobile-grid-100" name="vehicle_type">
                <option value="" selected>Select a type of vehicle</option>
                <option value="Shuttle Service">Shuttle Service</option>
                <option value="Taxi">Taxi</option>
                <option value="Bus">Bus</option>
                <option value="Jeepney">Jeepney</option>
                <option value="UV Express">UV Express</option>
                <option value="School Service">School Service</option>
                <option value="Truck for Hire">Truck for Hire</option>
                <option value="Tourist Bus">Tourist Bus</option>
                <option value="Tourist Car">Tourist Car</option>
                <option value="Uber">Uber</option>
                <option value="GrabCar">GrabCar</option>
              </select>
            </div>
          </div>

          <div class="input-group grid-100 tablet-grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
              Plate Number:
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">
              <input class="grid-100 tablet-grid-100 mobile-grid-100" type="text" name="plate_number"/>
            </div>
          </div>

          <div class="input-group grid-100 tablet-grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
            Operator Name:
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">
              <input class="grid-100 tablet-grid-100 mobile-grid-100" type="text" name="operator"/>
            </div>
          </div>

          <div class="input-group grid-100 tablet-grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
              Type of Complaint:
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">
              <select class="grid-100 tablet-grid-100 mobile-grid-100" name="complaint_type">

                <option value="" selected>Select a type of incident</option>

                <option value="Refusal to Convey passenger">Refusal to Convey passenger</option>

                <option value="Fast Meter">Fast Meter</option>

                <option value="Contracting passenger">Contracting passenger</option>

                <option value="Defective Meter">Defective Meter</option>

                <option value="Arrogant/Discourteous Driver">Arrogant/Discourteous Driver</option>

                <option value="Overcharging/Undercharging">Overcharging/Undercharging</option>

                <option value="No Flag Down meter">No Flag Down meter</option>

                <option value="Out of Line">Out of Line</option>

                <option value="Hit and Run">Hit and Run</option>

                <option value="Threatening  Passenger">Threatening  Passenger</option>

                <option value="Reckless Driving">Reckless Driving</option>

                <option value="Discriminating against passenger">Discriminating against passenger</option>

                <option value="Refusal to grant discount">Refusal to grant discount</option>

                <option value="Others">Others</option>

              </select>
            </div>
          </div>

          <div class="input-group grid-100 tablet-grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
              Date:
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">
              <input class="grid-100 tablet-grid-100 mobile-grid-100" type="text" name="date" placeholder="When did the incident happen?"/>
            </div>
          </div>

          <div class="input-group grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
              Location:
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">
              <div class="grid-100 grid-parent" id="locationField">
                <input class="grid-100 tablet-grid-100 mobile-grid-100" type="text" id="location" name="location" placeholder="Where did the incident happen?"/>
              </div>
            </div>
          </div>

          <div class="input-group grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
              Complaint:
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">
              <textarea rows="5"class="grid-100 tablet-grid-100 mobile-grid-100" type="text" name="description" placeholder="Optional. You can put here other important details that we might have missed."></textarea>
            </div>
          </div>

           <div class="input-group grid-100 tablet-grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
              Upload Photo
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">
              <p><input name="picture" type="file" value="Select File" /></p>
            </div>
          </div>

        </div>

        <!--Start of Contact Info-->
        <div class="form-body padded-bottom-20">
          <div class="heading-medium">2. Contact Info</div>
          <div class="input-group grid-100 tablet-grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
              Contact Number:
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">
              <input class="grid-100 tablet-grid-100 mobile-grid-100" type="number" name="contact"/>
              <div class="mobile-grid-100 grid-parent meta-text">
                We don't share this information with other users. We need this in case we need to get in touch with you in process of addressing your complaint.
              </div>
            </div>
          </div>
          <div class="input-group grid-100 tablet-grid-100 grid-parent">
            <div class="grid-25 tablet-grid-25 mobile-grid-100 form-label">
              Email Address:
            </div>
            <div class="grid-75 tablet-grid-75 mobile-grid-100">
              <input class="grid-100 tablet-grid-100 mobile-grid-100" type="email" name="email"/>
              <div class="mobile-grid-100 grid-parent meta-text">
                We don't share this information with other users. We need this in case we need to get in touch with you in process of addressing your complaint.
              </div>
            </div>
          </div>
      </div>
      <!--End of Contact Info-->


  </div>
  <div class="grid-100 grid-parent">
    <div class="grid-100 grid-parent input-group">
      <a class="btn-medium btn btn-default link" href="/">Cancel</a>
      <a class="btn-medium btn btn-green" id="submit-report">Submit</a>
  </div>
</div>
