
<div class="navigation grid-90 prefix-5">

  <div class="mobile-grid-100 navigation-inner">
    <div>
      <a class="link brand-logo" href="/"><img src="/static/app/images/roadmob-logo.png" height="28"></a>
    </div>
    <div class="search-button-group">
      <input id="roadmob-search-input" type="text" class="grid-100 mobile-grid-100 tablet-grid-100 input-rounded borderless" placeholder="Search for plate number or operator">
      <a id="roadmob-search-button" class="search-button btn btn-medium btn-green-inverse btn-rounded-right borderless">
        <i class="fa fa-search"></i></a>
    </div>

    <div id="links-right" style="padding-left: 10px">
      <a class="link hide-on-mobile" href="#about"> About </a>
      <!--<a class="link hide-on-mobile" href="#stats"> Stats </a>-->
      <% if (name) { %>
      <a class="link btn btn-large hide-on-mobile" href="/report/new" id="file-main">File A Report</a>
      <% } else { %>
        <a id="fb-login" class="link-button"><i class="fa fa-facebook"></i> Login</a>
      <% } %>
    </div>

    <div id="user-menu" style="display:none !important">
      <% if (name) { %>

        <div class="menu-button nav-right text-right">


          <div class=" header-user-picture" style="background-image: url('http://graph.facebook.com/<%= fb_id %>/picture?width=100&height=100')"></div>
          <div class="hide-on-mobile header-user-name"><%= name %></div>
        </div>

        <div class="menu-dropdown">
          <a class="logout">Logout</a>
        </div>

      <% } %>

      <!--<a class="link link-button btn" href="/report/new">File A Report</a>-->
      <!--<a class="link-button btn btn-normal-inverse" href={% url 'user:logout' %}>Logout</a>-->
    </div>
  </div>


</div>
