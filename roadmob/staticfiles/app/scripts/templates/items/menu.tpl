<% if (name) { %>
<div class="text-center">
  <div class="nav-account" style="background-image: url('http://graph.facebook.com/<%= fb_id %>/picture?width=100&height=100')"></div>
</div>

<% } %>
<a class="site-menu-close"><i class="fa fa-times"></i></a>
<a class="site-menu-link link" href="/report/new">File a report</a>
<a class="site-menu-link" href="/user/account">Profile</a>

<% if (name) { %>
<a class="site-menu-link" id="logout">Logout</a>
<% } %>
