<div class="hero-map"></div>
<div class="hero-overlay">
  <div class="hero-overlay__container">
    <h1>Help fellow commuters.<br />Report irresponsible drivers.</h1>
    <a href="/report/new" class="link btn btn-large" id="file-main">File A Report</a>
  </div>
</div>
