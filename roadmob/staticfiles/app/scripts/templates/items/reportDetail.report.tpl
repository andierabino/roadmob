<div class="grid-90 prefix-5">

	<div class="report-detail grid-60 prefix-20 tablet-grid-80 tablet-prefix-10">

		<div class="report-detail-inner">
			<a class="modal-close"><i class="fa fa-times"></i></a>
			<% if (picture) { %>
			<div class="report-detail-image grid-100 grid-parent">

				<img src="http://<%= picture %>" width="100%">

			</div>
			<% } %>
			<div class="heading-large"><%= plate_number %></div>
			<div class="heading-medium">
				<span class="tags-small"><%= incident %></span>
			</div>
			<p ><%= description %></p>
			<p class="text-meta">Posted on <%= date_text %></p>

			<div class="card-actions">
				<div class="grid-100 grid-parent">
					<a class="btn-small share-facebook btn btn-social btn-default-inverse btn-circle">
						<i class="text-small fa fa-facebook"></i></a>
					<a class="btn-small share-twitter btn-social btn btn-default-inverse btn-circle">
						<i class="text-small fa fa-twitter"></i>
					</a>
				</div>
			</div>



			<div class="heading-medium">Comments</div>
			<div class="div-loading comments-loading div-loading-rolling-orange"></div>
			<div class="grid-100 grid-parent">
				<div class="fb-comments" data-href="<%= REPORT_URI + id %>" data-numposts="10" data-width="100%">
				</div>
			</div>
		</div>
	</div>
</div>
