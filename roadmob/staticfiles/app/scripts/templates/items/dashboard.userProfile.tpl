<div class="grid-100 grid-parent dashboard-region v-padded-50">
  <div class="text-center grid-100">
    <div class="profile-picture" style="background-image: url('http://graph.facebook.com/<%= fb_id %>/picture?width=150&height=150')"></div>
  </div>
  <div class="text-center grid-100">
    <div class="grid-100 heading-medium"><%= name %></div>
    <div class="grid-100 text-meta"><div class="text-meta">Member since</div><%= registered_at %></div>
    <!--
    <p>
      <div class="grid-100 text-medium"><a class="btn-red btn btn-small" href="#">Edit Contact Info</a></div>
    </p>-->
  </div>
</div>
