<div class="dashboard-report-inner grid-parent grid-100">
  <div class="grid-90 prefix-5">
  	<div class="grid-100">

  		<div class="text-large report-title"><b><%= plate_number %></b></div>
  		<p>
  			<span class="tags-small"><%= incident %></span>
  		</p>
  		<p><%= description %></p>
      <% if (picture) { %>
      <div class="report-detail-image preview grid-p grid-100">
        <img src="http://<%= picture %>" width="100%">

      </div>
      <% } %>
  		<p class="text-meta">Posted on <%= date_text %></p>
  		<div class="">
  			<div class="grid-100 grid-parent">
  				<a class="btn-small share-facebook btn btn-social btn-default-inverse btn-circle">
  					<i class="text-small fa fa-facebook"></i></a>
  				<a class="btn-small share-twitter btn-social btn btn-default-inverse btn-circle">
  					<i class="text-small fa fa-twitter"></i>
  				</a>
  			</div>
  		</div>
  	</div>


  </div>
</div>
