define([
  'marionette',
  'backbone',
  'controller',
  'routerFilter',
  'underscore'
], function (Marionette, Backbone, controller, _routeFilter, _underscore) {
  'use strict';
  var _routeName, _routeArgs, referrer;
  return Marionette.AppRouter.extend({
    appRoutes: {
      '': 'home',
      'unbuilt': 'home',
      'report/new': 'reportCreate',
      'search(?:query_string)': 'reportSearch',
      'report/:id(?:query_string)' : 'reportDetail',
      'dashboard': 'dashboard'
    },
    controller: controller,
    before: {
      // Using inline filter definition and `return false` if don't want route to be executed
      'dashboard': 'checkAuthorization',
      '': 'checkAuthorization',
      'report/new': 'checkAuthorization'
    },
    onRoute: function (routeName, routePath, routeArgs) {
      referrer = _routeName;
      _routeName = routeName;
      _routeArgs = routeArgs;
    },
    getRouteName: function () {
      return _routeName;
    },
    getRouteArgs: function () {
      return _routeArgs;
    },
    getReferrer: function () {
      return referrer;
    },
    navigate: function (fragment, options, data) {
      controller.setData(data);
      if(fragment != '' && fragment != '/') {
        $('#header-region').removeClass('on-landing');
      }

      return Marionette.AppRouter.prototype.navigate.call(this, fragment, options);
    },
    checkAuthorization: function (fragment, args, next) {
      //If user is logged in, redirect home to dashboard
      if (G.me && !_.isEmpty(G.me)) {
        if(!fragment || fragment == '/') {
          Backbone.history.navigate('dashboard', {trigger:true, replace: false});
        } else {
          $('#header-region').removeClass('on-landing');
          next();
        }

      } else {

        $.ajax({
          url: '/api/login',
          success: function (response) {
            if (response.user) {
              if (fragment) {
                $('#header-region').removeClass('on-landing');
                next();
              } else {
                Backbone.history.navigate('dashboard', {trigger:true, replace: false});
                controller.dashboard();
              }
            } else {
                if(!fragment || fragment == '/') {
                  Backbone.history.navigate('', {trigger:true, replace: false});
                  controller.home();
                } else {
                  next();
                }

            }
          }
        })

      }
    }
  })
});
