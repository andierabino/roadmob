#-*- coding: utf-8 -*-
from .base import *

DEBUG = True

HOST = 'http://shielded-escarpment-4361.herokuapp.com/'

ALLOWED_HOSTS = ['shielded-escarpment-4361.herokuapp.com']

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'users',
    'reports',
    'oauth2_provider'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

AUTHENTICATION_BACKENDS = (
    # ... your other backends
    'roadmob.authbackends.PasswordlessAuthBackend',
    'django.contrib.auth.backends.ModelBackend',

)

# Parse database configuration from $DATABASE_URL
import dj_database_url
db_config =  dj_database_url.config()
if db_config:
    DATABASES['default'] =  db_config
