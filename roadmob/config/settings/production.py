#-*- coding: utf-8 -*-
from .base import *

DEBUG = False

HOST = 'http://www.roadmob.org/'

ALLOWED_HOSTS = ["herokuapp.com", ".roadmob.org"]


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'users',
    'reports',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

AUTHENTICATION_BACKENDS = (
    # ... your other backends
    'roadmob.authbackends.PasswordlessAuthBackend',
    'django.contrib.auth.backends.ModelBackend',

)

# Parse database configuration from $DATABASE_URL
import dj_database_url
db_config =  dj_database_url.config()
if db_config:
    DATABASES['default'] =  db_config
