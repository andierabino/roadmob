from django.contrib.auth.backends import ModelBackend

from users.models import User

class PasswordlessAuthBackend(ModelBackend):
    """Log in to Django without providing a password.

    """
    def authenticate(self, fb_id=None):
        print("using passwordless backend")
        try:
            #print("attempting to find user " + User.objects.all())
            user = User.objects.get(fb_id=fb_id)
            #print("found user " + user)
            return user
        except User.DoesNotExist:
            print("did not find user")
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
