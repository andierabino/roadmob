from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import django.utils.timezone as django_timezone

from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import pytz


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

def timezone_utc_date(date):
    tz = pytz.timezone('UTC')
    return django_timezone.make_aware(date, tz)

def timezone_aware_date(date):
    tz = pytz.timezone('Asia/Taipei')
    return django_timezone.make_aware(date, tz)

def datetext(date):
    return date.strftime("%B %d, %Y %I:%M %p")
