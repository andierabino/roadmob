[ ![Codeship Status for ohandie/roadmob](https://codeship.com/projects/a06a7e70-505a-0133-e0cd-3ae26323838a/status?branch=master)](https://codeship.com/projects/107647)
# How to setup application.

1.Clone repository.

2.Go to repository folder and create a virtual environment

For, Ubuntu and Elementary OS:

Update python
```bash
$ sudo apt-get build-dep python-psycopg2
```


```bash
$ virtualenv ENV -p /usr/bin/python3
```

For, Mac OS:

```bash
$ virtualenv ENV -p /usr/local/bin/python3
```

3.Activate virtual environment.

```bash
$ source ENV/bin/activate
```

4.Install app requirements.

```bash
$ pip install -r requirements.txt
```

5.Start your postgres

6.Create a database with name "roadmob". 

```bash
$ psql
$ CREATE DATABASE roadmob 
```

6.Run the server

```bash
$ python manage.py runserver
```

7.Go to your browser and visit localhost:8000